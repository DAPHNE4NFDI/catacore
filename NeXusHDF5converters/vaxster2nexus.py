#!/usr/bin/python3
from numpy import float32, float64, int32, int64, array, newaxis, sum, arange, nan


import glob
import fnmatch
import zipfile
import h5py
import os

h5rootattrs = {#"NeXus_version":"v2022.07", # "Only used when the NAPI has written the file."
                "HDF5_Version":h5py.version.hdf5_version,
                "h5py_version":h5py.__version__,
                "creator":"Insitute for Crystallography and Structural Physics, Friedrich-Alexander-Universität Erlangen-Nürnberg, Germany\nVAXSTER-to-NeXus converter, 2022-2023 jonas.graetz@fau.de",
                "creator_version":"2023-11" }

#https://manual.nexusformat.org/classes/base_classes/NXsource.html
nxsource = \
{   
    "name":    ("Excillum Liquid Metal Jet D2 d2lks1", {}),
    "type":    ("Metal Jet X-ray",                     {}),
    "probe":   ("x-ray",                               {}),
    "voltage": (float64(70),                           {'units':'keV', 'comment':'target value'}), #actually usually off slightly 
    "energy":  (float64(9.2426),                       {'units':'keV', 'comment':'weighted average Ga K_α1,2'}),  #Ga Kalpha(1) from https://physics.nist.gov/cgi-bin/XrayTrans/
    #"energy_errors":  (float64(0.02),                  {'units':'keV', 'date':'2022-10-10'})  #assessment from known k-alpha splitting and observed precision in AgBeh calibration,  cf. monochromator information
    #cf. monochromoator information
}
#https://manual.nexusformat.org/classes/base_classes/NXmirror.html
nxmirror = \
{
    "type":        ("multi",                                                                      {                   }),    # multi layer mirror. 
    "description": ("INCOATEC Custom ELM68 Montel multilayer mirror optic for Gallium radiation", {"date":"2015-07-20"}),
                    #LFE model with mirror housing (2 manual and 2 motorized alignment knobs).\nMirror design data:\na=1750 mm, b=9 mm, f=105.000, length=150.000,\ndesign wavelength = Ga K-alpha, figure error < 2 arcsec rms.\n2015-07-20",  {'date':'2015-07-20'}),   
                    # is this now the official product "INCOATEC HELIOS HR Ga Optics for Metaljet (https://www.incoatec.de/metaljet)" ?
}

# values should be re-evaluated for the current Ga/9keV source; 
# included here however (with the added reference to Cu/8.05kev) nevertheless as this literally is what the instrument control shows to the user.
vaxsterAttenuatorDict = {
    0       : (0,  "2mm Alu; beam blocked (home position) (0)"),
    -14760  : (1,  "without attenuation"),
    -29520  : (2,   "30um Al; Factor (Cu/8.05keV): 1.321"),
    -44280  : (3,   "50um Al; Factor (Cu/8.05keV): 1.563"),
    -59040  : (4,  "100um Al; Factor (Cu/8.05keV): 2.459"),
    -73800  : (5,  "200um Al; Factor (Cu/8.05keV): 5.994"),
    -88560  : (6,  "400um Al; Factor (Cu/8.05keV): 43.08"),
    -103320 : (7,  "800um Al; Factor (Cu/8.05keV): 1280")
}
vaxsterNxAttentuatorType = 'Al',
vaxsterAttenuatorTicknessDict = {
          0 : (2.,   {'units':"mm"}),
          1 : (0.,   {'units':"mm"}),
          2 : (0.03, {'units':"mm"}),
          3 : (0.05, {'units':"mm"}),
          4 : (0.1 , {'units':"mm"}),
          5 : (0.2 , {'units':"mm"}),
          6 : (0.4 , {'units':"mm"}),
          7 : (0.8 , {'units':"mm"})
}
vaxsterBeamstopDict = {
      65.00 : (0,  "beamstop out"),
     170.00 : (1,  "2mm beam stop centered"),
     143.00 : (2,  "2mm beam stop off-centered"),
       8.50 : (3,  "GISAXS beam stop (left bottom corner)"),
     230.00 : (4,  "4mm beam stop centered"),
     203.00 : (5,  "4mm beam stop off-centered"),
      -1.54 : (6,  "No Beamstop - Beam centered  (WARNING: Counter-clockwise hard limit on motor 1, unit 2, \"bstop\".)"), ##USE command bstop_out instead 
     110.00 : (7,  "6mm beam stop centered"),
      83.00 : (8,  "6mm beam stop off-centered")
}
vaxsterBeamstopSizeDict = {
          0 : (0. ,  {'units':"mm"}),
          1 : (2. ,  {'units':"mm"}),
          2 : (2. ,  {'units':"mm"}),
          3 : (4. ,  {'units':"mm"}),
          4 : (4. ,  {'units':"mm"}),
          5 : (4. ,  {'units':"mm"}),
          6 : (0. ,  {'units':"mm"}),
          7 : (6. ,  {'units':"mm"}),
          8 : (6. ,  {'units':"mm"})
}
vaxsterBeamstopDescription = 'circular' #cf. NXbeam_stop options.

vaxsterPositionerUnits = {
    "att":"setps",
    "bstop":"deg",
    "pd":"deg",
    "detx":"mm",
    "dety":"mm",
    "detz":"mm",
    "cstage":"deg",
    "pstage":"deg",
    "tstage":"deg",
    "ystage":"mm",
    "zgstage":"mm",
    "zkstage":"mm",
    "xsam":"mm",
    "zsam":"mm",
    "zsrc":"mm",
    "mirX":"mm",
    "mirY":"mm",
    "mirZ":"mm",
    "xcol":"mm",
    "zcol":"mm",
    "s1ig":"mm",
    "s1ip":"mm",
    "s1og":"mm",
    "s1op":"mm",
    "s2ig":"mm",
    "s2ip":"mm",
    "s2og":"mm",
    "s2op":"mm",
    "s3ig":"mm",
    "s3ip":"mm",
    "s3og":"mm",
    "s3op":"mm",
    "s4ig":"mm",
    "s4ip":"mm",
    "s4og":"mm",
    "s4op":"mm"
}

#https://manual.nexusformat.org/classes/base_classes/NXmonochromator.html
nxmonochromator = \
{ 
  #no "type" field? -- micro-focus source + multilayer mirror + slit collimators
  "type":       ("Montel optic for Gallium Bremsstrahlung / Ga K-alpha radiation",{                                                     }),
  "energy":     (float64(9.2426),                                                 {'units':'keV', 'comment':'weighted average Ga K_α1,2'}), #Ga Kalpha1 from https://physics.nist.gov/cgi-bin/XrayTrans/
  "wavelength": (float64(0.13414),                                                {'units':'nm', ' comment':'weighted average Ga K_α1,2'}), #computed from energy 9251.7  --> .13401 (K-alpha2: 9224.8 -->.13440)
  #"energy_errors"    : (float64(0.02),   {'units':'keV' }), # 
  #"wavelength_errors": (float64(0.0003), {'units':'keV' }), #uncertainty estiamted from K-alpha 1/2 splitting and multi-distance silver behenate calibration
  #"wavelength_spread": (float64(0.002),  {'units':'nm'  }), #guessed from general multilayer optics properties; required for NXsas. how does this relate to "wavelength_errors" ?
}

#https://manual.nexusformat.org/classes/base_classes/NXcollimator.html
#https://manual.nexusformat.org/classes/applications/NXsas.html#nxsas
#https://manual.nexusformat.org/classes/applications/NXcanSAS.html
#the NXcollimator class covers types: Soller | radial | oscillating | honeycomb
# .. i.e., analog to "scatter grids" parallel to the intended beam paths
#Anton Paar uses "kratky block"
#slits/pinholes are explicitly considered by NXaperture (cf. also NXcanSAS)
#nxcollimator = \
#{                             # 9.244 would be the average
#  "energy":            (float64(9.25),      {'units':'keV'}), #Ga Kalpha1 from https://physics.nist.gov/cgi-bin/XrayTrans/
#  "wavelength":        (float64(0.134),     {'units':'nm'}), #computed from energy 9251.7  --> .13401, 9224.8 -->.13440 
#  # (for angstrom units, apparently the literal word "angstrom" is used)   
#  #"geometry": shape (nxbox/nxcylinder), size   # NXcollimator deprecates this and expects NXtransformations and NXoff_geometry#
#
#  #"type": (used by anton paar; yet neither specified in NXsas nor in NXcollimator)    
#  #"collimation":"point": (used by anton paar; yet neither specified in NXsas nor in NXcollimator) 
#}


from numpy import clip, log1p, average, nan_to_num, inf, percentile, void
from matplotlib.pyplot import imsave
from matplotlib import cm
from io import BytesIO
def renderVaxsterThumbnail(detectorImage, format='jpeg'):
    img = detectorImage#array(h5py.File(h5filename)["/entry/data/data"])
    limg = nan_to_num(log1p(clip(img, 0, inf)))
    limg = clip(limg, 0, percentile(limg, 99.99))
    slimg = average(average(limg[:618, :486].reshape(206,3,162,3), axis=3), axis=1)
    slimg = clip(slimg, 0, percentile(slimg, 99.9))
    jpgblob = BytesIO()
    imsave(fname=jpgblob, arr=slimg, cmap=cm.viridis, format=format, pil_kwargs=dict(quality=75, optimize=True))
    jpgblob.seek(0)
    thumbnail = jpgblob.read()
    nxthumbnail = ( { 'data':(void(thumbnail), {}), 
                      'type':('image/%s'%format,{}),
                  }, {'NX_class':'NXnote'} )
    return nxthumbnail


import re
def parseSAXSlab(metadataString):
    """recursive parser for SAXSlab / Xenocs meta-data format.
2022-08 jonas.graetz@fau.de"""
    # the following regexp
    # contains three groups (each of which reduce to an empty string if not applicable):
    # (key-value item, section name, section body)
    # i.e.; for key-value items, the second and third entry will be an empty string
    #       for sections, the first will be an emptry string
    items = re.findall(r"((?m:(?<=^# )[^<>]+?$))|<([A-Z ]+) START>((?s:.*?))<\2 END>", metadataString)
    metaData = {}
    for item in items:
        if item[1] != '' and item[2] != '': # we have a section (i.e. "node"), make a recursive evaluation:
            metaData.update({item[1]:parseSAXSlab(item[2])})
        else:                               # we have a key-value item (i.e., "leave"), so insert it directly:
            metaData.update({ key:value
                              for key,value in re.findall("(.+?):\s+(.*?)\s*$", item[0]) } ) 
                              # iteration expected to be only over 1 element; 
                              # yet this formulation also gracely handles the case of empty sets.
    return metaData


def parseDectrisCommentField(s):
    """recursive parser for SAXSlab / Xenocs meta-data format,
2022-08 jonas.graetz@fau.de"""
    fields = {}
    for i, line in enumerate(s.splitlines()):
        line = line.lstrip("# ")
        if   ": "  in line: key, value = line.split(": " , 1)
        elif ", "  in line: key, value = line.split(", " , 1)
        elif " = " in line: key, value = line.split(" = ", 1)
        elif " "   in line: key, value = line.split(" "  , 1)
        else:               key, value = "line_%i" % (i+1), line
        fields.update({key:value})
    return fields


import datetime
def parseTiffDatetime(datetimeString):
    #print(datetimeString)
    if type(datetimeString) == bytes:
        datetimeString = datetimeString.decode('utf-8')
    return datetime.datetime.strptime(datetimeString,
                             "%Y:%m:%d %H:%M:%S").timestamp()


from PIL import Image 
def parseDectrisTIFF(file, translate=False):
    #exifread version:
    #with open(file, "rb") as fd:
    #    tags = exifread.process_file(fd)
    #    fields = parseDectrisCommentField(tags["Image ImageDescription"].values)
    #    fields.update({k:tags[k].values for k in ("Image Model",
    #                                             "Image Software",
    #                                             "Image DateTime")})
    
    #PIL version:
    with Image.open(file, "r") as img:
        tags = img.getexif()
        data = array(img)
        fields = parseDectrisCommentField(tags[270]) # Image ImageDescription
        exifkeys = {272:"Image Model", 305:"Image Software", 306:"Image DateTime"}
        fields.update({exifkeys[k]:tags[k].strip("\x00") for k in exifkeys.keys() if k in tags })
        #if fields["Retrigger_mode"] != "1":
        #    raise ValueError("Retrigger_mode != 1. ")
        if translate:
            pixelsize = float64(re.findall("[+]?[0-9]+\.?[0-9]*(?:e[+-]?[0-9]+)?(?= m)", 
                                           fields["Pixel_size"]))
            if (len(pixelsize) != 2):
                raise ValueError("cannot identify pixel size. Original string: %s" % fields["Pixel_size"])
            if (pixelsize[0] != pixelsize[1]):
                raise ValueError("Found non-square pixel-size. Original string: %s\nTODO: confirm x / y attribution" % fields["Pixel_size"])
            fields.pop("Pixel_size")
            metadata={ 
              # should different units than expected be used in the file / value strings,
              # the respective .strip(" unit characters") calls do not work as expected
              # leaving (other) unitstrings in the string, correctly causing an error at float/int conversion.
              "data":             (data,                                                  {'units':'counts'}),     #NXsas NXDetector convention (2d)
              #"data":             (data[newaxis, ...],                                    {'units':'counts'}),    #NeXus Base NXDetector convention (3d)
              "count_time":       (float64(fields.pop("Exposure_time").rstrip(" s")),     {'units':'s'}     ),
              "frame_time":       (float64(fields.pop("Exposure_period").rstrip(" s")),   {'units':'s'}     ),
              "saturation_value": (data.dtype.type(fields.pop("Count_cutoff").rstrip(" counts")),{}         ),
              "threshold_energy": (float64(fields.pop("Threshold_setting").rstrip(" eV")),{'units':'eV'}    ),
              "gain_setting":     (fields.pop("Gain_setting")                            ,{}                ),
              "x_pixel_size":     (pixelsize[0],                                          {'units':'m'}     ),
              "y_pixel_size":     (pixelsize[1],                                          {'units':'m'}     ),
              "description":      (fields["Image Model"].split(",",1)[0].strip(),         {}                ),
              "serial_number":    (fields.pop("Image Model").split("S/N",1)[1].strip(),   {}                ),
              "flatfield_name":   (fields["Flat_field"].strip(),                          {}                ),
              "flatfield_applied":((fields.pop("Flat_field").strip().strip("()").strip().lower() \
                                    not in [ "0", "off", "nil" ]),                        {}                ),
              "countrate_correction_lookup_table_name":(fields["Ratecorr_lut_directory"].strip(),{}     ),
              "countrate_correction_applied":((fields.pop("Ratecorr_lut_directory").strip().strip("()").strip().lower() \
                                    not in [ "0", "off", "nil" ]),                        {}                ),
              }    
            try:
                metadata.update({"start_time":(int64(round(parseTiffDatetime(fields["Image DateTime"])-metadata['frame_time'][0])), {'units':'s'})}) 
                metadata.update({"stop_time": (int64(round(parseTiffDatetime(fields.pop("Image DateTime")))), {'units':'s'})})
            except KeyError:
                pass
            # add vendor name to description; hardcoded as we are in the "parseDectris" function anyway. 
            if "dectris" not in metadata["description"][0].lower():
                metadata.update({"description":("DECTRIS %s" % metadata["description"][0], metadata["description"][1])})
            # add detector type; as we are introducing new meta-data fields, only do so for known detector types.
            if "pilatus" in metadata["description"][0].lower() or \
               "eiger"   in metadata["description"][0].lower():
                metadata.update({"layout":("area",{}), "type":("photon counting", {})})
            # add readout bits, as we are introducing new meta-data fields, only do so for known detectors
            if "3-0166-500Hz" in metadata["serial_number"][0]:
                metadata.update({"bit_depth_readout":(20, {})})
            # sensor type encoded in the key name; sensor thickness in the corresponding value
            # look for "... sensor" key (currently "Silicon sensor" or "CdTe sensor" are known values)
            # and extract thickness with some allowance for different numeric / unit formats.
            sensorTypeKey = "" # initialize to empty as fallback
            for key in fields.keys():
                if re.match("[a-z0-9_-]+ sensor", key.lower()):
                    sensorTypeKey = key
                    material = re.findall("([a-zA-Z0-9_-]+) sensor", sensorTypeKey)[0]
                    if material.lower() == "silicon": material = "Si"
                    metadata.update({"sensor_material":(material,{})})
                    if re.match("thickness[:= ]+[+]?[0-9]*\.?[0-9]*(?:e[+-]?[0-9]+)?[ ]*[a-zµ]+",
                                fields[sensorTypeKey].lower()):
                        thickness, thickness_unit = re.findall("[Tt]hickness[:= ]+([+]?[0-9]*\.?[0-9]*(?:e[+-]?[0-9]+)?)[ ]*([a-zA-Zµ]+)",
                                                               fields.pop(sensorTypeKey))[0]
                        thickness = float64(thickness)
                        metadata.update({"sensor_thickness":(thickness, {"units":thickness_unit})})
                    break # only one sensor description field will be there, so stop here. 
            for key in fields.keys():
                if not re.match("line_[0-9]+", key):
                    metadata.update({"DECTRIS_%s" % key:(fields[key],{})})
            return metadata
            #'N_excluded_pixels': '6',
            #'Excluded_pixels': 'badpixel_mask.tif',                        --> pixel_mask, pixel_mask_applied
            #'Flat_field': 'FF_p3-0166-500Hz_E8048_T4024_vrf_m0p100.tif',   --> flatfield, flatfield_applied
            #                                ... vrf_m0p100 refers to gain. currently, the gain setting is autog vrf=1.000; i.e. vrf_p1p000?
            #'Trim_file': 'p3-0166-500Hz_E8048_T4024.bin',                  --> threshold energy calibration
            #'Image_path': '/data/datatemp/',
            #'Ratecorr_lut_directory': 'Continuous',                        --> count_rate_correction_applied, countrate_correction_lookup_table
            # #https://media.dectris.com/user-manual-pilatus3-2020.pdf page 44 (camserver command reference)
            # #"rateCorrLUTDirectory is pathname or off; Disabling LUT based correction"
            # #"The detector is shipped with a correction for a continuous X-ray beam.""
            
            #'Retrigger_mode': '1', 
            #
            # 
            
        else:
            return fields




def dict2hdf5(h5, data):
  if type(h5) == str:
    with h5py.File(h5, "w") as fd:
      dict2hdf5(fd, data)
  else: #assuming h5 is a h5py File descriptor
    for key, (value, attributes) in data.items():
      if type(value) == dict:
        g = h5.create_group(name=key)
        g.attrs.update(attributes)
        dict2hdf5(g, value)
      else:
        d = h5.create_dataset(name=key, data=value)
        d.attrs.update(attributes)


def VAXSTER2NeXus(frames_path, output_path=False, caz_tiff=False):
    assert re.match("(fr_[0-9]{7}_raw)(.zip)?", os.path.basename(frames_path))
    identifier = re.findall("[0-9]+", frames_path)[-1]
    
    # open file and load data and metadata using preivously defined parser functions
    if frames_path.endswith(".zip"):
        with zipfile.ZipFile(frames_path, 'r') as f:
            filelist = f.namelist()
            fnglob = lambda pattern: fnmatch.filter(filelist, pattern)
            tiffs = sorted(fnglob("fr_%s_[0-9][0-9][0-9][0-9][0-9].tiff" % identifier))
            if len(tiffs) == 0:  # if only a single frame is present, numbering will be absent:
                tiffs = sorted(fnglob("fr_%s_.tiff" % identifier))
            saxslab_metadata = parseSAXSlab(f.open("metadata_%s.txt" % identifier).read().decode('utf-8'))
            frames_data      = [ parseDectrisTIFF(f.open(file, 'r'), translate=True) for file in tiffs ]
    else:
        tiffs = sorted(glob("%s/fr_%s_[0-9][0-9][0-9][0-9][0-9].tiff" % (frames_path, identifier)))
        if len(tiffs) == 0:  # if only a single frame is present, numbering will be absent:
            tiffs = sorted(glob("%s/fr_%s_.tiff" % (frames_path, identifier)))
        saxslab_metadata = parseSAXSlab(open(glob("%s/metadata_%s.txt"  % (frames_path, identifier))[0]).read())
        frames_data      = [ parseDectrisTIFF(file, translate=True) for file in tiffs ]
    
    #extract aqusition information to be computed from all frames 
    final_stop_time  = frames_data[-1]['stop_time']
    start_time       = frames_data[0]['start_time']
    total_count_time = (float64(sum([ f['count_time'][0] for f in frames_data ])), frames_data[-1]['count_time'][1])
    cumulative_frame_time = (float64(sum([ f['frame_time'][0] for f in frames_data ])), frames_data[-1]['count_time'][1])
    total_frame_time = (max(final_stop_time[0] - start_time[0], cumulative_frame_time[0]), start_time[1])
    total_data       = (sum([f["data"][0] for f in frames_data], axis=0).astype(int32), frames_data[-1]['data'][1])

    #initialize detector with information computed from all frames
    nxdetector = \
    {
        'number_of_cycles' : (int64(len(frames_data)), {}),
        'count_time'       : total_count_time, # attributes contained in total_count_time
        'frame_time'       : total_frame_time, # analog ..
        'stop_time'        : final_stop_time,  # ...
        'start_time'       : start_time,
        'data'             : total_data,
    }
    
    #include streak / zinger (due cosmic radiation events) corrected image, if present / if provided:
    if caz_tiff:
      try:
        with Image.open(caz_tiff, "r") as caz_img:
          data = array(caz_img)
          nxdetector.update({"zinger_filter_applied":(True, {"description":"suppression of high energy cosmic ray events detected in sub-frames"})})
          nxdetector['data'] = (data, nxdetector['data'][1])
      except FileNotFoundError:
        print("WARNING: %s not found, ignoring..." % caz_tiff)
        nxdetector.update({"zinger_filter_applied":(False, {"description":"suppression of high energy cosmic ray events detected in sub-frames"})})
    
    #add static (detector) information/settings from first frame
    nxdetector.update({ key:frames_data[0][key] for key in frames_data[0] if key not in nxdetector })
    
    #remove or update single-frame saturation_value for cumulative images? Could be misleading.
    #also: is the saturation_value field updated (by dectris) when the flatfield is applied?
    #if nxdetector['number_of_cycles'][0] > 1:
    #    if 'saturation_value' in nxdetector:
    #        nxdetector['saturation_value'] = (nxdetector['saturation_value'][0] * nxdetector['number_of_cycles'][0],
    #                                          nxdetector['saturation_value'][1])
    
    #remove saturation value, as it is unreliable for the Dectris. 1) The value is only meaningful in the absence of flatfield correction; and 2) it is ill-defined for a cumulative sum of images
    nxdetector.pop('saturation_value', None);
        
    nxpositioners = { k:(float64(v), {'units':vaxsterPositionerUnits[k]} if k in vaxsterPositionerUnits else {})
                      for k, v in saxslab_metadata["SAXSLAB METADATA"]["SPEC METADATA"].items() }
    nxpositioners.pop('test', None) # remove orphaned "test" spec variable, if present


    #####  create actual HDF5 and fill the structure based on the above dictionaries:
    
    nxthumbnail = renderVaxsterThumbnail(nxdetector['data'][0])
    
    f5filename = "%s%s.h5" % (output_path+"/" if output_path else '', identifier)
    f5 = h5py.File(f5filename, 'w')
    f5.attrs.update(h5rootattrs)
    f5.attrs.update({"NX_class"  : "NXroot"            ,
                     "default"   : "entry"             ,
                     "file_time" : ""                  ,
                     "file_name" : "%s.h5" % identifier})

    dict2hdf5(f5, {'entry' : ({}, { 'NX_class' : 'NXentry', 
                                    'default'  : 'data'   ,
                                  #'definition':'NXsas', # not all information available, especially beamcenter and wavelength_spread; 
                                  })})
    
    #required for NXsas:
    #dict2hdf5(h5nxenxtry, {'title',     ('', {}),
    #                       'start_time',('', {}),
    #                       'stop_time', ('', {}) })    #required for NXsas
     
    dict2hdf5(f5['/entry'], { 'data'          : ({}, {'NX_class' : 'NXdata', 'signal':'data', 
                                                     #'axes' : [".", "y", "x"], # if first is n, things break. probably issue with dimensionlength 1?
                                                      'axes' : ["y", "x"],
                                                      'interpretation' : 'image', 
                                                      'x_indices':1, 'y_indices':0}), # redundant with @axes attribute with the additional option to add alternative dimensioncales for a particular data array index; sees to be ignored by silx; reguired by NXdata standard; 
                              'thumbnail'     : nxthumbnail,
                              'instrument'    : ({
                                  'name'          : ('VAXSTER',       {                                      }),
                                  'source'        : (nxsource,        {'NX_class' : 'NXsource'               }),
                                  'mirror'        : (nxmirror,        {'NX_class' : 'NXmirror'               }),
                                  'monochromator' : (nxmonochromator, {'NX_class' : 'NXmonochromator'        }),
                                  'detector'      : (nxdetector,      {'NX_class' : 'NXdetector'             }),
                                  'positioners'   : ({}, {'NX_class' : 'NXcollection'           }),
                                  #'attenuator'    : ({}, {'NX_class' : 'NXattenuator'           }), # will be added further down, provided attenuator raw position interpretation is contained in vaxsterAttenuatorDict
                                  #'collimator'    : ({}, {'NX_class' : 'NXcollimator'           }), # collimator .. mirror? or slits? NXcollimator specifically assumes one of : "Soller | radial | oscillating | honeycomb", none of which applies here. 
                                  #'aperture'      : ({}, {'NX_class' : 'NXaperture'             }), # intended to be used to document slit/pinhole (collimators).
                                                                                                     # the interpretations of the contents will be so instrument specific that we assume the user to directly consult instrument/positioners/s[i|o][1-4][g|p]
                                  #NXgeometry: legacy class. use NXtransformation instead
                                  #'geometry'      : ({'shape':({},{'NX_class':'NXshape'})}, {'NX_class' : 'NXgeometry'             }),
                               }, {'NX_class' : 'NXinstrument'           }),
                              #'sample'        : ({}, {'NX_class' : 'NXsample'               }), # information not available from VAXSTER frame files
                              #'user'          : ({}, {'NX_class' : 'NXuser'                 }), # information not available from VAXSTER frame files
                             })
    # hard-link actual data from NXinstrument/NXdetector:
    f5["/entry/data/data"] = f5["/entry/instrument/detector/data"]
    # add axes to NXdata:
    dict2hdf5(f5['/entry/data'], { "y" : (arange(619)*0.172, {'units':'mm', 'long_name':'y [mm]'}), #-- silx view doesn't use the units attribute to generate axis labels
                                   "x" : (arange(487)*0.172, {'units':'mm', 'long_name':'x [mm]'})  #-- silx view doesn't use the units attribute to generate axis labels
                                 })
    # insert positioner information, considering special cases for zsrc, zcol, zsam, xcol, xsam, att, bstop. 
    # For all others ("else" path), assume motor in physical units without encoder.
    h5positioners = f5["/entry/instrument/positioners"]
    for name, (value, attrs) in nxpositioners.items():
        dict2hdf5(h5positioners, {name:({'name':(name, {})}, {'NX_class':'NXpositioner'})})
        g = h5positioners[f"{name}"]
        if name in [ "zsrc", "zcol", "zsam" ]: # motor with encoder
            dict2hdf5(g, {'value'        : (array((value,), dtype=float64), attrs),
                          'value_set'    : (array((value,), dtype=float64), attrs)})
        elif name in [ "xcol", "xsam" ]: # only encoder, manual motion
            dict2hdf5(g, {'value'        : (array((value,), dtype=float64), attrs)})
        elif name in [ "att", "bstop" ]: # raw stepper motor counts (att), degrees (bstop), without encoder. Add as raw_value_set and add (cf. later) the respective enumerated settings as visible to the user
            dict2hdf5(g, {'raw_value_set': (array((value,), dtype=float64), attrs)})
        else: #motor without encoder
            dict2hdf5(g, {'value_set'    : (array((value,), dtype=float64), attrs)})
    
    # add human-readable translations for (raw) att motor positions according to vaxsterAttenuatorDict and vaxsterAttenuatorThicknessDict
    attConfig = int(nxpositioners["att"][0])
    if attConfig in vaxsterAttenuatorDict:
        attNumber = vaxsterAttenuatorDict[attConfig][0]
        dict2hdf5(h5positioners["att"]   , { "value_set"             : (array((attNumber                   ,)), { 'units' : '(enumeration)'}),
                                             "value_set_description" : (vaxsterAttenuatorDict[attConfig][1], {                          })
                                           })
        # add formal NXattenuator entry:
        dict2hdf5(f5['/entry/instrument'], { 'attenuator' : ({ 'status'   : ('out' if vaxsterAttenuatorTicknessDict[attNumber][0] == 0 else 'in', {}),
                                                               'thickness': vaxsterAttenuatorTicknessDict[attNumber],
                                                               'type'     : (vaxsterNxAttentuatorType, {})},
                                                             {'NX_class' : 'NXattenuator'})
                                           }) 
        
    # add human-readable translations for (raw) bstop motor value according to vaxsterBeamstopDict
    bstopConfig = round(float(nxpositioners["bstop"][0]),2)
    if bstopConfig in vaxsterBeamstopDict:
        bstopNumber = vaxsterBeamstopDict[bstopConfig][0]
        bstopSize   = vaxsterBeamstopSizeDict[bstopNumber]
        dict2hdf5(h5positioners["bstop"], { "value_set"             : (array((bstopNumber,), dtype=int64) , { 'units' : '(enumeration)'}),
                                            "value_set_description" : (vaxsterBeamstopDict[bstopConfig][1], {                          })
                                          })
        dict2hdf5(f5['/entry/instrument'], { 'beamstop' : ({ 'status'      : (('in' if bstopSize != 0 else 'out'), {}),
                                                             'size'        : vaxsterBeamstopSizeDict[bstopNumber],
                                                             'description' : (vaxsterBeamstopDescription, {}),
                                                             'comment'     : (vaxsterBeamstopDict[bstopConfig][1],{})},
                                                           {'NX_class':'NXbeam_stop'})
                                            })
    else:
        dict2hdf5(h5positioners["bstop"], { "value_set"             : (array((-1,), dtype=int64)          , { 'units' : '(enumeration)'}),
                                            "value_set_description" : ("custom manual configuration"      , {                          }),
                                          })
        dict2hdf5(f5['/entry/instrument'], { 'beamstop' : ({ 'status'      : ('unknown' , {}),
                                                             'size'        : (nan       , {}),
                                                             'description' : ((vaxsterBeamstopDescription, {}), {}),
                                                             'comment'     : ('manual/custom configuration', {})},
                                                           {'NX_class':'NXbeam_stop'})
                                           }) 

    # finalize and return
    f5.close()
    return f5filename

import sys
if __name__ == "__main__":
    in_path  = sys.argv[1]
    out_path = sys.argv[2] if len(sys.argv) > 2 else False
    caz_path = sys.argv[3] if len(sys.argv) > 3 else None
    VAXSTER2NeXus(in_path, out_path, caz_path)


