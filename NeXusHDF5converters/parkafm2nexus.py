#!/usr/bin/python3
import re
import sys, h5py, datetime
from numpy import array, frombuffer, average, dtype, void, float32, float64, uint8, uint16, linspace
from os.path import basename, exists
from PIL import Image
from io import BytesIO


#https://github.com/mdendzik/Park-AFM-data-viewer/blob/master/AFMimage.m
#https://github.com/christian-sahlmann/gwyddion/blob/b0a16600d36fca015c2c3aabda489ea38db0e969/modules/file/psia.c
#

instrumentName = "Park Systems NX10 AFM";
h5rootattrs = {#"NeXus_version":"v2022.07", # "Only used when the NAPI has written the file."
                    "HDF5_Version":h5py.version.hdf5_version,
                    "h5py_version":h5py.__version__,
                    "creator":"Insitute for Crystallography and Structural Physics, Friedrich-Alexander-Universität Erlangen-Nürnberg, Germany\nParkAFM-to-NeXus converter, 2022-2023 jonas.graetz@fau.de",
                    "creator_version":"2023-12" }

parkafm=\
 [('nImageType',         '<i4'),   #0   0=2d mapped image, 1= line profile image, 2=Spectroscopy image
  ('szSourceNameW',      'S64'),   #4   Source name (topography, ZDetector, itp)
  ('szImageModeW',       'S16'),   #68  Image mode (AFM...)
  ('dfLPFStrength',      '<f8'),   #84  Low pass filter strength
  ('bAutoFlatten',       '<i4'),   #94  Automatic flatten after imaging
  ('bACTrack',           '<i4'),   #96  AC track
  ('nWidth',             '<i4'),   #100 Number of columns
  ('nHeight',            '<i4'),   #104 Number of rows
  ('dfAngle',            '<f8'),   #108 Angle of fast direction about positive x-axis
  ('bSineScan',          '<i4'),   #116 Sine Scan
  ('dfOverScan',         '<f8'),   #120 OverScan rate
  ('nFastScanDir',       '<i4'),   #128 (bFastScanDir) Fast scan direction(non-zero for forward, 0 for backward); note 2023-12-14: apparently not changed for forward/backward in recent ParkSystems SmartScan software
  ('nSlowScanDir',       '<i4'),   #132 Slow scan direction(non-zero for up, 0 for down)
  ('bXYSwap',            '<i4'),   #136 Swap fast-slow scanning direction
  ('dfXScanSizeum',      '<f8'),   #140 X scan size in um
  ('dfYScanSizeum',      '<f8'),   #148 Y scan size in um
  ('dfXOffsetum',        '<f8'),   #156 X offset in um
  ('dfYOffsetum',        '<f8'),   #164 Y offset in um
  ('dfScanRateHz',       '<f8'),   #174 Scan speed in rows per second
  ('dfSetPoint',         '<f8'),   #182 Error signal set point
  ('szSetPointUnitW',    'S16'),   #188 Set point unit
  ('dfTipBiasV',         '<f8'),   #204 Tip Bias voltage in V
  ('dfSampleBiasV',      '<f8'),   #212 Sample bias voltage in V
  ('dfDataGain',         '<f8'),   #220 Data gain < 0 UnitW/step; Data=DataGain*(dfScale*nData+dfOffset)
  ('dfZScale',           '<f8'),   #228 Z scale now it is always 1
  ('dfZOffset',          '<f8'),   #236 Z offset i steps, now always 0 
  ('szUnitW',            'S16'),   #244 Unit of Data gain per step
  ('nDataMin',           '<i4'),   #260 DataMax < DataMin because DataGain < 0  # "-1"; not used anymore ? at least not for f4 data! cf. also "ZScale" & "ZOffset"
  ('nDataMax',           '<i4'),   #264 DataMax < DataMin because DataGain < 0  # " 0"; not used anymore ? at least not for f4 data!
  ('nDataAvg',           '<i4'),   #268 Average Data                            # " 0"; not used anymore ? at least not for f4 data!
  ('nCompression',       '<i4'),   #272 Compession option, not used yet
  ('bLogScale',          '<i4'),   #276 Is the data in log scale
  ('bSquare',            '<i4'),   #280 Is the data squared
  ('dfZServoGain',       '<f8'),   #284 Z-Servo gain
  ('dfZScannerRange',    '<f8'),   #292 Z scanner Range
  ('szXYVoltageMode',    'S16'),   #300 XY Voltage mode
  ('szZVoltageMode',     'S16'),   #316 Z voltage Mode
  ('szXYServoMode',      'S16'),   #332 XY servo mode
  ('nDataType',          '<i4'),   #248 Data Type 0=16bitshort, 1= 32bit int, 2= 32bit float
  ('bXPDDRegion',        '<i4'),   #252 # of X PDD region
  ('bYPDDRegion',        '<i4'),   #256 # of Y PDD region
  ('dfNCMAmplitude',     '<f8'),   #360 Non Contact Mode Amplitude
  ('dfNCMFrequency',     '<f8'),   #368 Non Contact Mode Frequency
  ('dfHeadRotationAngle','<f8'),   #376 Head Rotation Angle
  ('szCantilever',       'S16'),   #284 Cantilever name
  ('dfNCMDrivePercent',  '<f8'),   #400 Non Contact Mode Drive %, range= 0-100
  ('dfIntensityFactor',  '<f8'),   #408 intensity factor (dimensionless) = (A+B)/3
  
  # reverse enginnered:
  # currently missing values found in the GUI:
  # "Flattening (Offset Adj.)" (as opposed to AutoFlatten (Plain Fit)) -- this might not even be an extra field here
  # stage coordinate and sample coodrinate 
  # "Head Tilt Angle" ( as opposed to Head Rotation Angle)
  
  ('dfDrive',            '<f8'),   #416 "Drive" in percent; as displayed in the XEI GUI
  ('dfUnknown01',        '<f8'),   #424 value around one, similar to dfDrive
  ('dfUnknown02',        '<f8'),   #432 value found in test file: 0    
  ('dfUnknown03',        '<f8'),   #440 value found in test file: 0
  ('dfUnknown04',        '<f8'),   #448 value found in test file: 50
                                       
                                   
  
  ('dfZServoNCGain?',    '<f8'),  # always 1 both in XEI viewer and here, independent of actual setting
  ('dfZServoPGain?',     '<f8'),  # always 1 both in XEI viewer and here, independent of actual setting
  ('dfZServoIGain?',     '<f8'),  # always 1 both in XEI viewer and here, independent of actual setting
  
  ('nbUnknown08',        '<i4'),   # 1, might be a boolean
  ('dfUnknown09',        '<f8'),   # 5.143676407745734e-06
  ('dfUnknown10',        '<f8'),   #-5.143676407506002e-06
  ('dfUnknown11',        '<f8'),   # 0
  ('dfUnknown12',        '<f8'),   # 0  
  ('dfUnknown13',        '<f8'),   # 0
  ('dfUnknown14',        '<f8'),   # 0
  ('dfUnknown15',        '<f8'),   # 5.689847569215322
  ('dfUnknown16',        '<f8'),   # 0
  ('dfUnknown17',        '<f8'),   # 0
  ('dfUnknown18',        '<f8'),   # 0
  ('dfUnknown19',        '<f8'),   # 0
  ('dfUnknown20',        '<f8'),   # 0
  
 ]

#not yet used. Mapping between struct attribute names and displayed names in the XEI GUI
attrDisplayNames={'ImageMode':     'Head Mode',        #string
                'XYVoltageMode': 'XY Range Mode',    #string
                'ZVoltageMode':  'Z Range Mode',     #string
                'Cantilever':    'Cantilever',       #string
                'SourceName':    'Source',           #string
                'LPFStrength':   'Low Pass Filter',  #value
                #'AutoFlatten':   'Plain Fit', (both are boolean)
                #'??':           'Flattening' ("Offset Adj.") -- probably a default/invariable entry, given that ZScale and ZOffset are apparently fixed to 1 and 0 ?
                'SineScan':      'Sine Scan',        #boolean
                'OverScan':      'Over Scan',        #percent
                'XYServoMode':   'XY Servo Mode',    #boolean/string
                #'XYSwap':           'Fast Scan Axis', -- are up/down back/front to be swapped then as well? -- yes! confirmed with example images
                'FastScanDir':   'Fast Scan Dir',    #boolean/string
                'SlowScanDir':   'Slow Scan Dir',    #boolean/string
                'XScanSizeum':   'X Scan Size',      #um
                'YScanSizeum':   'Y Scan Size',      #um
                'XOffsetum':     'X Scan Offset',    #um
                'YOffsetum':     'Y Scan Offset',    #um
                'Angle':         'Rotation',         #degree
                'ScanRateHz':    'Scan Rate',        #Hz
                'ZServoGain':    'Z Servo Gain',     #value
                'SetPoint':      'Set Point',        #nm
                'NCMAmplitude':  'Amplitude',        #nm
                'NCMFrequency':  'Sel. Frequency',   #Hz
                #'NCMDrivePercent': None,   # not displayed in XEI? no correspondence found
                #'IntensityFactor': None,   # not displayed in XEI? no correspondence found
                'HeadRotationAngle': 'Head Rotation Angle',     #deg
                #'??':            'Head Tilt Angle',  #deg
                'TipBiasV':       'Tip Bias',        #V
                'SampleBiasV':    'Sample Bias',     #V
                'DataGain':       'Data Gain',       #UnitW/step
                'Drive':          'Drive',           #percent
                'ZServoNCGain':   'Z Servo NC Gain', #value
                'ZServoPGain':    'Z Servo P Gain',  #value
                'ZServoIGain':    'Z Servo I Gain',  #value
                }


def parkafm2dict(data):
    transformations={'<i4':lambda v:   int(v), #integer
                     '<f4':lambda v: float(v), #float
                     '<f8':lambda v: float(v), #double
                     'S16':lambda v:       v[::2].decode('utf-8'), #string
                     'S64':lambda v:       v[::2].decode('utf-8'), #string
                     ('b',32):lambda v:    v,
                     ('b',100):lambda v:   v}
    transformations2={'b':lambda v: v>0, #boolean (stored as integer)
                      's':lambda v: v,   #string
                      'd':lambda v: v,   #double
                      'n':lambda v: v,   #integer
                      'e':lambda v: v}   #"empty"
    def _addUnitAttributes(meta):
        for key in list(meta.keys()):
            if key[-2:] == "um" or key[-2:] == "Hz":
                meta.update({key:(meta[key], {"units":key[-2:]})})
            elif key[-1:] == 'V':
                meta.update({key:(meta[key], {"units":key[-1:]})})
            elif "Angle" in key:
                meta.update({key:(meta[key], {"units":"deg"})})
        meta["NCMFrequency"] = (meta["NCMFrequency"], {"units":"Hz"})
        meta["DataGain"]     = (meta["DataGain"],     {"units":"%s/step" % meta["Unit"]})
        meta["SetPoint"]     = (meta["SetPoint"],     {"units":meta["SetPointUnit"]})
        meta["Width"]        = (meta["Width"],        {"units":"px"})
        meta["Height"]       = (meta["Height"],       {"units":"px"})
        meta["OverScan"]     = (meta["OverScan"]*100, {"units":'%'}) #initial value is fraction
        meta["Drive"]        = (meta["Drive"],        {"units":'%'})
        meta["NCMDrivePercent"] = (meta["NCMDrivePercent"], {"units":'%'}) 
    
        
    nImageTypeEnum = {0:'2d mapped image', 
                      1:'line profile image',
                      2:'spectroscopy image'}
    nDataTypeEnum  = {0:'<i16', 
                      1:'<i32',
                      2:'<f4'}
    
    #('bFastScanDir',       '<i4'),   #128 Fast scan direction(non-zero for forward, 0 for backward)
    #('nSlowScanDir',       '<i4'),   #132 Slow scan direction(non-zero for up, 0 for down)
    
    meta = { key.lstrip('bfszdn').rstrip('W'):transformations2[key[0]](transformations[t](data[key])) for key, t in parkafm }
    #meta.pop('empty')
    meta['ImageType']    = (meta['ImageType'], nImageTypeEnum[meta['ImageType']])
    meta['DataType']     = (meta['DataType'],  nDataTypeEnum[meta['DataType']])
    
    _addUnitAttributes(meta)
        
    if meta['XYSwap']: meta.update({'FastScanAxis':'Y'})
    else:              meta.update({'FastScanAxis':'X'})
    
    # the case "not meta['FastScanDir']" should reflect the "Backwards" direction, the value is however never actually changed between forward/backward mode images (in recent park system files as of 2020-2023)    
    if (    meta['FastScanDir']) and (not meta['XYSwap']): meta['FastScanDir'] = (meta['FastScanDir'], "Left to Right")
    if (not meta['FastScanDir']) and (not meta['XYSwap']): meta['FastScanDir'] = (meta['FastScanDir'], "Right to Left") 
    if (    meta['FastScanDir']) and (    meta['XYSwap']): meta['FastScanDir'] = (meta['FastScanDir'], "Bottom to Top")
    if (not meta['FastScanDir']) and (    meta['XYSwap']): meta['FastScanDir'] = (meta['FastScanDir'], "Top to Bottom")

    
    if (    meta['SlowScanDir']) and (not meta['XYSwap']): meta['SlowScanDir'] = (meta['SlowScanDir'], "Bottom to Top")
    if (not meta['SlowScanDir']) and (not meta['XYSwap']): meta['SlowScanDir'] = (meta['SlowScanDir'], "Top to Bottom")
    if (    meta['SlowScanDir']) and (    meta['XYSwap']): meta['SlowScanDir'] = (meta['SlowScanDir'], "Left to Right")
    if (not meta['SlowScanDir']) and (    meta['XYSwap']): meta['SlowScanDir'] = (meta['SlowScanDir'], "Right to Left")
   
    

    #'AutoFlatten' == 'Plain Fit', or 'AutoFlatten' == 'Flattening' ("Offset Adj.")
    #probably 'Flattening' might be part of the new fields?
    #if meta['AutoFlatten']:     meta['AutoFlatten'] = (meta['AutoFlatten'], 'Off')
    #else:                       meta['AutoFlatten'] = (meta['AutoFlatten'], 'On')
    
    
   
    return meta


    

def readParkAFMimage(filePath):
    """reads proprietary Park AFM TIFF format, extracting full resolution image, TIFF thumbnail image and instrument metadata.,
2023-01 -- 2023-12 jonas.graetz@fau.de"""
    with Image.open(filePath) as tiff:
        exif = tiff.getexif()
        meta = parkafm2dict(
              frombuffer( exif[50435], 
                          dtype=dtype(parkafm) )[0] )
        data = frombuffer( exif[50434], 
                           dtype=dtype(meta['DataType'][1]) ).reshape(meta['Height'][0], meta['Width'][0])
        
        # LogScale, ZScale and ZOffset have been used to efficiently store images as (8bit?) 16bit integers
        # in recent versions, the data is directly stored in float format and the values default to ZScale=1, ZOffset=0, LogScale=False;
        data=( meta['DataGain'][0] * (meta['ZScale'] * data + meta['ZOffset']) ).astype(float32)
        if meta['LogScale']: raise NotImplementedError("Data is stored in logscale. Transformation not yet implemented.")
        if meta['ZScale'] != 1 or meta['ZOffset'] != 0: raise NotImplementedError("ZScale != 1 or ZOffset != 0 have not yet been explicitly tested.")
        
        #keep for completeness:
        #meta.pop('DataGain')
        #meta.pop('ZScale')
        #meta.pop('ZOffset')
        
        for i in range(0, 21):
          meta.pop('Unknown%02i'%i, None)
        for gain in ('I', 'NC', 'P'):
          meta.pop('ZServo%sGain?' % gain, None)
        datetime_isoformat = datetime.datetime.strptime(exif[306], "%Y:%m:%d %H:%M:%S").strftime("%Y-%m-%dT%H:%M:%S")
        meta.update({'InstrumentName':exif[305], 'DateTime':datetime_isoformat, 'FileName':basename(filePath)})
        if 320 in exif:
          meta.update({'Colormap':(array(exif[320], dtype=uint16).reshape(3, 256), {'format':'(3, 2^bitsPerSample) values for red, green, blue as 16bit unsigned integer values'})})
        return (data, meta)
        


def dict2hdf5(h5, data):
  if type(h5) == str:
    with h5py.File(h5, "w") as fd:
      dict2hdf5(fd, data)
  else: #assuming h5 is a h5py File descriptor
    for key, (value, attributes) in data.items():
      if type(value) == dict:
        g = h5.create_group(name=key)
        g.attrs.update(attributes)
        dict2hdf5(g, value)
      else:
        try:
          d = h5.create_dataset(name=key, data=value)
          d.attrs.update(attributes)
        except TypeError:
          #print(key)
          #print(value)
          raise

# helper function to convert arbitrary strings to valid NeXus identifiers
#2023 jonas.graetz@fau.de"""

# https://manual.nexusformat.org/datarules.html#naming-conventions
# allowed names:  any combination of upper and lower case letter, numbers, underscores and periods, 
#                 except that periods cannot be at the start or end of the string
# recommended:    lower case words separated by underscores and, if needed, with a trailing number

# ad-hoc translation table for accented vocals and and a few more:
# (there will certainly be more capable libraries for the purpose of accent stripping)
ACCENTS = \
    [ ("àáâãäå", "a"), ("èéêë", "e"), ("ìíîï", "i"), ("òóôõö", "o"), ("ùúûü", "u"), ("ýÿ", "y"), ("ñ", "n"), ("ç", "c") ]
ACCENTS_TRANSLATION_TABLE = \
    "".maketrans("".join([a[0] + a[0].upper()  for a in ACCENTS]) + "ß", 
                 "".join([a[1] * (len(a[0])*2) for a in ACCENTS]) + "S")

def getValidNeXusIdentifier(string):
    """Translate and/or remove "non-NeXus" characters.
cf. allowed NeXus itentifiers: https://manual.nexusformat.org/datarules.html#naming-conventions
"""
    identifier = re.sub("[^A-Za-z0-9.]+", "_", string.translate(ACCENTS_TRANSLATION_TABLE)).strip('.')
    if len(identifier) > 63:
        raise ValueError("identifier exceeds 63 characters")
    return identifier

#helper function to handle the heterogeneity of data entries (with / without further attributes, etc)
#that emerged in the course of the development / enrichtment of metadata
def getvalue(data):
    if type(data) == tuple: 
        if type(data[1]) == dict: return data[0] #case we stored data as (value, attributesDict)
        else:                     return data[1] #case we stored data as (rawvalue, interpretedvalue)
    else:                         return data    #case we stored data just as (value)
def getattrs(key, data):
    attrs = {}
    try:   attrs.update({'long_name':attrDisplayNames[key]})
    except KeyError: pass
    if type(data) == tuple \
    and type(data[1]) == dict: attrs.update(data[1])
    elif type(data) == tuple: attrs.update({'raw_value':data[0]})
    return attrs

def parkfile2nexusentry(input_filename, output_hdf5_handle, collection_identifier=None):
  if input_filename[-4:] == '.jpg' and 'Vision' in basename(input_filename):
      return parkvisionjpeg2nexusentry(input_filename, output_hdf5_handle, collection_identifier=collection_identifier)
  if input_filename[-5:] == '.tiff':
      return parktiff2nexusentry(input_filename, output_hdf5_handle, collection_identifier=collection_identifier)

def parkvisionjpeg2nexusentry(input_filename, output_hdf5_handle, collection_identifier=None):
  f5 = output_hdf5_handle
  entry_name = 'Vision'
  #img = (average(average(array(Image.open(input_filename)).reshape(480, 2, 640, 2, 3).astype(float32), axis=3), axis=1)).astype(uint8)
  #dict2hdf5(f5, {entry_name : ({}, { 'NX_class' : 'NXdata', 
  #                                   'signal'  : 'image' }
  #                            ) 
  #              }
  #          )
  #f5entry = f5[entry_name]
  #dict2hdf5(f5entry, {'image':(img, {'interpretation':'rgb-image'})})
  
  
  dict2hdf5(f5, {entry_name: ({ 'data': (void(open(input_filename,'rb').read()),{}),
                                'type': ('image/jpeg',{})
                              }, {'NX_class':'NXnote'})
                }
           )
  return entry_name
  

def parktiff2nexusentry(input_filename, output_hdf5_handle, collection_identifier=None):
  """reads&parses the input ParkAFM TIFF file, and adds a NeXus entry to the provided HDF5 file handle.
2023-12 jonas.graetz@fau.de"""
  f5 = output_hdf5_handle
  data, meta = readParkAFMimage(input_filename)
  meta['InstrumentName'] = instrumentName;
  # as the meta['FastScanDir'] field is apparently not written appropriately, we must rely on the filename to identify forward/backward directions:
  # this 
  forward = 'Forward'     in basename(input_filename) and 'Backward' not in basename(input_filename)
  neither = ('Forward' not in basename(input_filename) and 'Backward' not in basename(input_filename)) or ('Forward' in basename(input_filename) and 'Backward' in basename(input_filename))
  entry_name = getValidNeXusIdentifier(meta['SourceName']) + ('' if neither else ('_FW' if forward else '_BW'))
  
  #in case forward/backward could not be cleanly identified, this can become helpful:
  if ('/%s' % entry_name) in output_hdf5_handle:
    entry_name = entry_name + '_2'
    
  thumbnail  = array(Image.open(input_filename))
  
  
  jpgblob = BytesIO()
  Image.open(input_filename).convert('RGB').save(jpgblob, format='jpeg', quality=75, optimize=True)
  jpgblob.seek(0)
  jpgthumbnail = jpgblob.read()
  
  nxthumbnail = ( { 'data':(void(jpgthumbnail), {}), 
                    'type':('image/jpeg',{}),
                  }, {'NX_class':'NXnote'} )
  
  {entry_name: ({ 'data': (void(open(input_filename,'rb').read()),{}),
                                'type': ('image/jpeg',{})
                              }, {'NX_class':'NXnote'})
                }
  
  xaxis = linspace(0, meta['XScanSizeum'][0], data.shape[1], False, dtype=float32)
  yaxis = linspace(0, meta['YScanSizeum'][0], data.shape[0], False, dtype=float32)
  
  
                   
  dict2hdf5(f5, {entry_name : ({}, { 'NX_class' : 'NXentry', 
                                     'default'  : 'data' }
                              ) 
                }
            )
  f5entry = f5[entry_name]
  
  #data=( meta['DataGain'][0] * (meta['ZScale'] * data + meta['ZOffset']) ).astype(float32)
  
  
  dict2hdf5(f5entry, { 'data'   : ({'x':(xaxis,{'long_name':'x [µm]', 'units':'µm'}),
                                    'y':(yaxis,{'long_name':'y [µm]', 'units':'µm'}),
                                   }, { 'NX_class' : 'NXdata', 'signal':'data', 
                                        'axes' : ["y", "x"],
                                        'interpretation' : 'image', 
                                        'x_indices':1, 'y_indices':0}), # redundant with @axes attribute with the additional option to add alternative dimensioncales for a particular data array index; sees to be ignored by silx; reguired by NXdata standard; 
                       'end_time'      : (meta.pop('DateTime'),{}),
                       'filename'      : (meta.pop('FileName'),{}),
                       #'thumbnail'     : (thumbnail, {'interpretation' : 'image'}),
                       'thumbnail'     : nxthumbnail,
                       'instrument'    : ({'name':(meta.pop('InstrumentName'),{}),
                                           'data':(data, {'units' : meta['Unit'], 'interpretation' : 'image'}),
                                           'metadata':({k:(getvalue(v), getattrs(k,v)) for k,v in meta.items()},{'NX_class':'NXcollection'})
                                          }, {'NX_class':'NXinstrument'}),
                                
                     }
           )
                           
  f5entry['data/data'] = f5entry['instrument/data']
  if collection_identifier is not None: f5entry['collection_identifier'] = collection_identifier
  if 'Colormap' in meta:
    f5entry['data/colormap'] = f5entry['instrument/metadata/Colormap']
    
  return entry_name

DEFAULT_PRIORITIES = [ 'Z_Height_FW', 'Z_Height', 'Z_Height_BW',
                       'NCM_Amplitude_FW', 'NCM_Amplitude', 'NCM_Amplitude_BW' ]
                       
def park2nexus(input_filenames, output_filename):
  """reads&parses the list of input ParkAFM TIFF files, creates an HDF5/NeXus file and adds an NXentry for each of the provided TIFF files.
2023-12 jonas.graetz@fau.de"""
  if exists(output_filename):
    raise ValueError("file %s already exists!" % output_filename)
    
  with h5py.File(output_filename, 'w') as f5:
    f5.attrs.update(h5rootattrs)
    if type(input_filenames) == str:
      input_filenames = (input_filenames,)
                     
    entry_names = [ parkfile2nexusentry(filename, f5) for filename in input_filenames ]
    
    if len(entry_names) == 0:
      default = ''
    elif len(entry_names) == 1:
      default = entry_names[0]
    else:
      default = entry_names[0]
      for d in DEFAULT_PRIORITIES:
        if d in entry_names: 
          default = d
          break
      
    f5.attrs.update({"NX_class"  : "NXroot"     ,
                     "default"   : default      ,})
                     
    
    
    
  
  
#from nexusformat.nexus import *
#def getNX(filename):
#    data, meta = readParkAFMimage(filename)
#    meta['InstrumentName'] = instrumentName; 
#    thumbnail  = array(Image.open(filename))
#    xaxis = linspace(0, meta['XScanSizeum'][0], data.shape[1], False, dtype=float32)
#    yaxis = linspace(0, meta['YScanSizeum'][0], data.shape[0], False, dtype=float32)
#    nx = \
#    NXroot(
#      attrs={'default':'/entry/data'},
#      entry=NXentry(name='entry',
#                    datetime   = NXfield(meta.pop('DateTime')),
#                    filename   = NXfield(meta.pop('FileName')),
#                    instrument = NXinstrument(name      = 'instrument', 
#                                              entries   = {'name':NXfield(name  = 'name',
#                                                                          value = meta.pop('InstrumentName'))},
#                                              data      = NXfield(value = data, 
#                                                                  attrs = {'units'          : meta['Unit'],
#                                                                           'interpretation' : 'image'}),
#                                              thumbnail = NXfield(value = thumbnail,
#                                                                  attrs = {'interpretation' : 'image'}),
#                                              metadata  = NXcollection(name    = 'metadata',
#                                                                       entries = { k:NXfield(value = getvalue(v),
#                                                                                             name  = k,
#                                                                                             attrs = getattrs(k, v)) 
#                                                                                   for k,v in meta.items() }
#                                                                       )
#                                             )
#                   )
#    )
#    nx['/entry/data'] = NXdata(name  = 'data',
#                               attrs = { 'signal':'data', 
#                                         'axes':['y','x'] },
#                               data  = NXlink(nx.entry.instrument.data),
#                               x     = NXfield(value=xaxis, attrs={'long_name':'x [µm]', 'units':'µm'}),
#                               y     = NXfield(value=yaxis, attrs={'long_name':'y [µm]', 'units':'µm'}),
#                               )
#    return nx


if __name__ == '__main__':
  input_paths  = sys.argv[1:-1]
  output_path = sys.argv[-1]
  #nx = getNX(input_path);  #nx.save(output_path)
  park2nexus(input_paths, output_path)
  
