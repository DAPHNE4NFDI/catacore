# -*- coding: utf-8 -*-
"""
Created on Fri Jul 21 11:04:03 2023

Authors: johannes.dallmann@fau.de, jonas.graetz@fau.de
"""
import sys
import os
import re
import numpy as np
import h5py
import datetime
from io import StringIO



def autocast(value):
    #cast every string representing a float to a float, int to and int and remove '"' from strings
    value = value.strip(' "')
    for f in (int, float, str):
        try:
            return f(value)
        except ValueError:
            continue

def read_data(filepath):
    """Returns everything in ras file as single string"""
    if not os.path.exists(filepath):
        raise Exception(f"Filepath {filepath} not found") 
    if os.stat(filepath).st_size == 0:
        raise Exception(f"File {filepath} is empty")
    fullstring = ""
    with open(filepath, "r", encoding="latin-1") as file:
        fullstring = file.read()
    return fullstring

def format_metastring(string):
    """Convert metadatastring into flat key, value dictionary for each row"""
    meta_list = [[autocast(col) for col in l.split(" ",1)] for l in string.splitlines()] 
    meta_dict = {c[0].strip("*").lower(): c[1] for c in meta_list}
    meta_dict["meas_scan_start_time"] = dateParser(meta_dict["meas_scan_start_time"])
    meta_dict["meas_scan_end_time"] = dateParser(meta_dict["meas_scan_end_time"])
    return meta_dict

def format_datastring(string):
    """Convert datastring into 2d numpy array"""
    return np.genfromtxt(StringIO(string)) if string != "" else np.array([])

def identify_sections(fullstring):
    """
    Returns list of dictionaries (sections). Each dictionary has:
        "meta": flat dictionary of key, value pairs in the header
        "raw_data": 2d numpy array of floats with shape (n,3) with x,y,z-axis as columns
                     Each entry in the 3rd column is always 1, contains no information
        "raw_temperaturedata": 2d numpy array of floats with shape (n,2) with x,y-axis as columns
    No information from the file is altered
    """
    
    section_delimiter = "*RAS_TEMPERATURE_END\n" #marks the end of a section
    sections = [s+section_delimiter for s in fullstring.split(section_delimiter)]
    #assert file endswith RAS_DATA_START to make sure it is complete
    assert sections[-1].startswith("*RAS_DATA_END"), "Incomplete datafile: No *RAS_DATA_END found"
    del sections[-1] #Remove last line: "*RAS_DATA_END"
    
    get_between = lambda string,start,end: string[string.find(start)+len(start):string.find(end)]
    
    #assert no START-END subsection is missed
    assert len(re.findall("\*RAS_[^ \r\n]+_START", sections[0])) > 3, "Unmatched Subsection found"
    
    sections = [{
        "meta": format_metastring(
            get_between(sectionstring, "*RAS_HEADER_START\n", "*RAS_HEADER_END\n")
        ),
        "raw_data": format_datastring(
            get_between(sectionstring, "*RAS_INT_START\n", "*RAS_INT_END\n")
        ),
        "raw_temperaturedata": format_datastring(
            get_between(sectionstring, "*RAS_TEMPERATURE_START\n", "*RAS_TEMPERATURE_END\n")
        ),
    } for sectionstring in sections]
    
    return sections

def merge_sections(sections):
    """
    merge all metadictionaries into one with list for values, if the respective value changed across sections
    merge all dataarrays along a new dimension
    return single section dictionary with identical keywords as individual sections before
    """
    metadatalist = [s["meta"] for s in sections]
    collapse = lambda lst: lst if len(set(lst)) > 1 else lst[0]
    return {
        "meta": {k: collapse([dic[k] for dic in metadatalist]) for k in metadatalist[0]},
        "raw_data": np.stack([s["raw_data"] for s in sections]),
        "raw_temperature": np.stack([s["raw_temperaturedata"] for s in sections]),
    }
    
def dateParser(datetimeString):
    # reads the observed format "01/18/19 20:26:23"
    # and returns it in standard ISO format "%Y-%m-%dT%H:%M:%S"
    return datetime.datetime.strptime(datetimeString, "%m/%d/%y %H:%M:%S").strftime("%Y-%m-%dT%H:%M:%S") 




def extract_axes_group(metadata):
    #summarize all belonging to one axis in subdictionary
    axeskeys = [k for k in metadata if k.startswith("meas_cond_axis_name_internal-")]
    axeskeys = sorted(axeskeys, key=lambda x: int(x.split("-")[1]))
    axesdata = {}
    for i,k in enumerate(axeskeys):
        #make sure units are in the separate unit field, not part of the positon value
        pos = metadata[f"meas_cond_axis_position-{i}"]
        unit = metadata[f"meas_cond_axis_unit-{i}"]
        # pos, unit = autocast2(pos)
        replace_units= ["mm"]
        for u in replace_units:
            if unit == "" and isinstance(pos, str) and pos.endswith(u):
                pos = autocast(pos.strip(u))
                unit = u
                break
        axesdata[metadata[k]] = (
            pos, 
            {
                "name": metadata[f"meas_cond_axis_name-{i}"],
                "magicno": metadata[f"meas_cond_axis_name_magicno-{i}"],
                "offset": metadata[f"meas_cond_axis_offset-{i}"],
                "units": unit,
            }
        )
    return (axesdata, {"NX_class": "NXcollection"})

def extract_source_group(metadata, axes):
    source = {
        "type": ("Rotating Anode X-ray" if metadata["hw_xg_type"] == "Rota" else metadata["hw_xg_type"], {}),
        "probe": ("x-ray",{}),
        "current": (metadata["meas_cond_xg_current"],{"units": metadata["hw_xg_current_unit"]}),
        "voltage": (metadata["meas_cond_xg_voltage"], {"units": metadata["hw_xg_voltage_unit"]}),
        "target_material": (metadata["hw_xg_target_name"], {}),
        "monochromator": axes[0]["IncidentPrimary"],
        "filter": axes[0]["Filter"],
    }
    return (source, {"NX_class": "NXsource"})

def extract_instrument_group(section):
    axes = extract_axes_group(section["meta"])
    source = extract_source_group(section["meta"],axes)
    metadata = {k: (v,{}) for (k,v) in section["meta"].items() if not k.startswith("meas_cond_axis_")}
    # scan = extract_scan_params(section)
    instrument = {
        "name": ("RigakuSmartLab", {}),
        "axes": axes,
        "source": source,
        "metadata": (metadata, {"NX_class": "NXcollection"}),
        # "scan": (scan, {"NX_class": "NXcollection"}),
    }
    return (instrument, {"NX_class": "NXinstrument"})

def extract_times(section):
    start_time = section["meta"]["meas_scan_start_time"] if type(section["meta"]["meas_scan_start_time"]) != list else section["meta"]["meas_scan_start_time"][0]
    end_time = section["meta"]["meas_scan_end_time"] if type(section["meta"]["meas_scan_start_time"]) != list else section["meta"]["meas_scan_start_time"][-1]
    duration = datetime.datetime.strptime(end_time,"%Y-%m-%dT%H:%M:%S") - datetime.datetime.strptime(start_time,"%Y-%m-%dT%H:%M:%S")
    return start_time,end_time, int(duration.total_seconds())

def extract_scan_params(section):
    scan = {
        "axis": (section["meta"]["meas_scan_axis_x_internal"], {}),
        "speed": (section["meta"]["meas_scan_speed"], {"units": section["meta"]["meas_scan_speed_unit"]}),
        "range_start": (section["meta"]["meas_scan_start"], {"units": section["meta"]["meas_scan_unit_x"]}),
        "range_end": (section["meta"]["meas_scan_stop"], {"units": section["meta"]["meas_scan_unit_x"]}),
        "steps": (section["meta"]["meas_scan_step"], {"units": section["meta"]["meas_scan_unit_x"]}),
    }
    return (scan, {"NX_class": "NXcollection"})


def extract_general_data_group(section, instrument):
    x_axis = section["raw_data"][:,0]
    x_axis_name =  section["meta"]["meas_scan_axis_x"]
    x_unit = section["meta"]["meas_scan_unit_x"]
    
    int_axis = section["raw_data"][:,1]
    int_axis_name = "intensity"
    int_unit = section["meta"]["meas_scan_unit_y"]
    
    temperature = "Temperature" in instrument[0]["axes"][0]
    temp_axis_name = "temperature"
    temp_axis = section["raw_temperaturedata"][:,1] if temperature else np.array([])
    temp_unit = instrument[0]["axes"][0]["Temperature"][1]["units"] if temperature else "-"
    temp_axis_name = "temperature"

    
    data = {
        "intensity": (int_axis, {"units": int_unit, "long_name": f"{int_axis_name} [{int_unit}]"}),
        "x": (x_axis,{"long_name": f"{x_axis_name} [{x_unit}]"}),
        "temperature": (temp_axis, {"long_name": f"{temp_axis_name} [{temp_unit}]"}),
    }

    return (data, {"NX_class": "NXdata", "axes": ["x"], "interpretation": "spectrum", "signal": "intensity"})
    

def extract_polefigure_data_group(section, instrument):
    
    all_x_axis = section["raw_data"][:,:,0]
    #assert scan axes are identical
    assert all([np.array_equiv(all_x_axis[0], a) for a in all_x_axis]), "scan axes are not identical"
    beta_axis = all_x_axis[0]
    beta_name =  section["meta"]["meas_scan_axis_x"]
    beta_unit = section["meta"]["meas_scan_unit_x"]
    
    alpha_axis = np.array(instrument[0]["axes"][0]["Alpha"][0])
    alpha_name =  "alpha"
    alpha_unit = instrument[0]["axes"][0]["Alpha"][1]["units"]
    
    
    int_axis = section["raw_data"][:,:,1]
    int_axis_name = "intensity"
    int_unit = section["meta"]["meas_scan_unit_y"]
    
    temperature = "Temperature" in instrument[0]["axes"][0]
    temp_axis_name = "temperature"
    temp_axis = section["raw_temperaturedata"][:,1] if temperature else np.array([])
    temp_unit = instrument[0]["axes"][0]["Temperature"][1]["units"] if temperature else "-"
    temp_axis_name = "temperature"

    
    data = {
        "intensity": (int_axis, {"units": int_unit, "long_name": f"{int_axis_name} [{int_unit}]"}),
        "beta": (beta_axis, {"long_name": f"{beta_name} [{beta_unit}]"}),
        "alpha": (alpha_axis, {"long_name": f"{alpha_name} [{alpha_unit}]"}),
        "temperature": (temp_axis, {"long_name": f"{temp_axis_name} [{temp_unit}]"}),
    }
    return (data, {"NX_class": "NXdata", "axes": ["alpha","beta"], "interpretation": "image", "signal": "intensity"})

def create_entry_group(section, filetype):
    instrument = extract_instrument_group(section)
    
    data_extractors = {
        "general":  extract_general_data_group,
        "polefigure": extract_polefigure_data_group,
    }
    
    data = data_extractors[filetype](section, instrument)
    start_time,end_time, duration = extract_times(section)
    
    entry = {
        "data": data,
        "start_time": (start_time, {}),
        "end_time": (end_time, {}),
        "duration": (duration, {"units": "s"}),
        "instrument": instrument,
    }
    return entry


def dict_to_h5(node, data):
    try:
        for key, (value, attributes) in data.items():
            if type(value) == dict:
                grp = node.require_group(key)
                grp.attrs.update(attributes)
                dict_to_h5(grp, value)
            else:
                dset = node.create_dataset(key, data=value)
                dset.attrs.update(attributes)
    except ValueError:
        print(data)
        
        
def convert_ras_file(filepath, filepath_output=None):
    fullstring = read_data(filepath)
    sections = identify_sections(fullstring)
    
    filetype = "general"
    if "file_data_type" in sections[0]["meta"] and sections[0]["meta"]["file_data_type"] == "RAS_3DE_POLEFIG":
        filetype = "polefigure"
        
    if filetype == "polefigure":
        sections = [merge_sections(sections)]
    
    entries = [create_entry_group(s, filetype) for s in sections]


    nexus = {}
    firstentry = "entry"
    if len(entries) == 1:
        nexus["entry"] = (entries[0], {"NX_class": "NXentry", "default": "data"})
    else:
        firstentry = "entry1"
        for i,entry in enumerate(entries):
            nexus[f"entry{i+1}"] = (entry, {"NX_class": "NXentry", "default": "data"})
    if filepath_output is None:
      filepath_output = filepath.replace(".ras", ".h5")
    with h5py.File(filepath_output, "w") as file:
        dict_to_h5(file, nexus)
        file.attrs.update({'default':firstentry})
        
if __name__ == "__main__":
    input_path = sys.argv[1]
    output_path = sys.argv[2] if len(sys.argv) > 2 else None
    try:
      convert_ras_file(input_path, output_path)
    except AssertionError:
      print(input_path)
      pass
        
