from time import sleep, time
from glob import glob
import os

def fileMonitorLoop(monitorGlobPattern, fileHandler, ageThresholdSeconds=30, loopIntervalSeconds=10, excludeList=[]):
    """file monitor:  check for new files and check for modifications of the most recent file
call `fileHandler(file)` for each file that is either not the latest one or has not been modified for at least `ageThresholdSeconds` seconds.
ingore any file(path)s in `excludeList` (e.g., files that have been handled in previous invocations of `fileMonitorLoop()`)
2023-01-15 jonas.graetz@fau.de, DAPHNE4NFDI project"""
    
    assert type(ageThresholdSeconds) in [ int, float ]
    assert type(loopIntervalSeconds) in [ int, float ]
    assert all([ type(e) == str for e in excludeList])
    excludeList = list(map(os.path.normpath, excludeList))     # ensure that equivalent pathes become identical
    monitorGlobPattern = os.path.normpath(monitorGlobPattern)  # otherwise files may not be skipped correctly (typically: double slashes vs single slashes)
    
    # initialize state variables
    prevFiles = excludeList 
    latest = latest_prev_mtime = latest_prev_mtime_timestamp = None
    # loop to look for new-and-finalized scans / framesets:
    while True:
        loop_timestamp = time()         # to define the sleep time prior to the next iteration
        curFiles = sorted(glob(monitorGlobPattern))
        newFiles = sorted(set(curFiles).difference(set(prevFiles)))
        prevFiles = curFiles
        if len(newFiles) > 0: # if new files arrived, the "latest" file will be finalized
            if latest: # Only handle if latest != None
                fileHandler(latest) 
                latest = None           # not strictly required, leave for clarity of "resetting" the latest file
            latest = newFiles[-1]       # track the new "latest" file
            #latest_mtime = os.path.getmtime(latest)  # will be set in the next step anyway
            latest_prev_mtime = latest_prev_mtime_timestamp = None
            for newFile in newFiles[:-1]: fileHandler(newFile) # all but the latest file are also finalized
        if latest: # if latest != None
            latest_mtime, latest_mtime_timestamp = os.path.getmtime(latest), time()
            if latest_mtime == latest_prev_mtime: # i.e. if "latest" file has not been updated since last check
                # compare the time since last check of mtime, as opposed to comparing with mtime directly,
                # to avoid issues with unsynchronized clocks between different systems:
                if (latest_mtime_timestamp - latest_prev_mtime_timestamp) > ageThresholdSeconds:  # "latest" file has not been updated anymore, so it is finalized
                    fileHandler(latest)
                    #"reset" all state variables for completeness:
                    latest = latest_mtime = latest_mtime_timestamp = latest_prev_mtime = latest_prev_mtime_timestamp = None
                else: # "latest" file wasn't updated since last check, but is not yet "old" enough to be finalized for sure
                  pass  # thus, do nothing and wait for next iteration
            else: # "latest" file was updated since last check, track the timestamps
                latest_prev_mtime_timestamp  = latest_mtime_timestamp
                latest_prev_mtime            = latest_mtime
        sleep(min(max(0, loopIntervalSeconds-(time()-loop_timestamp)), loopIntervalSeconds))
