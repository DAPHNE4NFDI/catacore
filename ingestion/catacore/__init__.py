import os
from numpy import array, isscalar, size, ndarray
import psycopg2 as pg
try:    import pandas as pd
except: print("WARNING: pandas not found, insert_dataframe() and insert_spreadsheet() will not be available")
from getpass import getpass
import json
from catacore.scientific_metadata import scientific_metadata  # https://github.com/SciCatProject/pyscicat/blob/main/pyscicat/hdf5/scientific_metadata.py

AVAILABLE_COLUMNS = None; # will be fetched from database at time of first ingestion


def defaultTypeConverter(input_):
  if isscalar(input_):
    try:
      return int(input_)
    except ValueError, TypeError:
      try:
        return float(input_)
      except: ValueError, TypeError:
        return str(input_)
  else:
    if size(input_) == 1:
      return defaultTypeConverter(array([input_.squeeze()])[0])
    elif type(input_) == ndarray:
      return input_.tolist() # also converts elements to python types
    else:
      return [ defaultTypeConverter(element) for element in input_ ]


def ingest(dbc, instrumentID, scanID, defaultScanIDformat='%07i', **kwargs):
    """ingest row into catacore eln.scanlog with primary key (instrumentID, scanID) and further columns defined in **kwargs),
    2023-09 jonas.graetz@fau.de"""

  
    scanID = scanID if type(scanID) == str else defaultScanIDformat % scanID

    global AVAILABLE_COLUMNS # initialize on first call of ingest():
    if AVAILABLE_COLUMNS is None: 
      AVAILABLE_COLUMNS = get_table_columns('sys.scanlog_ingestion_stage');

  
    userMetadata = { k:kwargs[k] for k in kwargs.keys() if k.lower() not in AVAILABLE_COLUMNS }
    if len(userMetadata) == 0: userMetadata = None
    if userMetadata is not None:
      assert 'metadata' not in kwargs or kwargs['metadata'] is None
      kwargs.update(dict(metadata=json.dumps(userMetadata, sort_keys=True, indent=2, default=defaultTypeConverter)))
    
    providedColumnsList = [ col for col in kwargs.keys() 
                            if col.lower() in AVAILABLE_COLUMNS + ['instrumentid', 'scanid'] ] 
    #everything else is by now already summarized in 'metadata'. add ['instrumentid', 'scanid' here for the corner case of respective kwargs with alternate upper/lower combinations
    
    #special treatment of "timestamp" in order to allow for both text and unix epoch reprentations
    insertColumnsString = ', '.join(providedColumnsList)
    insertValuesString  = ', '.join([ ('%s' if not (col == 'timestamp' and type(kwargs[col]) != str) else 'to_timestamp(%s)::timestamp')
                                      for col in providedColumnsList ])
    
    #will be handled as "conservative upsert" in the backend, i.e., all fields that are NULL will be updated.
    #typical usecase: row (instrumendID, scanID) has already been created by user with first annotations, is now completed with/by instrument meta-data
    #in case the ingestor provides (in **kwargs) (user-meta-data-)columns that conflict with already existing entries in the database, the existing entries will have priority.
    dbc.execute(f"""INSERT INTO sys.scanlog_ingestion_stage(instrumentID, scanID, { insertColumnsString })
                    VALUES(%s,%s,{insertValuesString});""", [instrumentID, scanID] + [kwargs[col] for col in providedColumnsList])
    #else: 
    #  insert(dbc, 'eln.scanlog', updateExisting=False, defaultScanIDformat=defaultScanIDformat, instrumendID=instrumendID, scanID=scanID, **kwargs)
                      

def ingest_hdf5(dbc, hdf5file, instrumentID, scanID, skipKeyList=["data"], defaultScanIDformat='%07i', **kwargs):
  """ingest row into catacore eln.scanlog with primary key (instrumentID, scanID), instrumentMetadata from hdf5file and further columns defined in **kwargs),
    use skipKeyList to ignore hdf5 sections that are not instrumentMetadata, e.g. actual data sections.
    2023-09 jonas.graetz@fau.de"""
  instrumentMetadata = json.dumps(scientific_metadata(hdf5file, skipKeyList = skipKeyList), indent=2)
  kwargs.update({'instrumentMetadata':instrumentMetadata})
  ingest(dbc, instrumentID, scanID, defaultScanIDformat=defaultScanIDformat, **kwargs)
  
 
def _format_scanid(rowDictionary, defaultScanIDformat='%07i'):
    # handle special case of scanID column:
    try: 
      scanIDkey = [ k for k in rowDictionary if k.lower() == 'scanid' ][0]
      rowDictionary[scanIDkey] = defaultScanIDformat % int(rowDictionary[scanIDkey])
    except (IndexError, ValueError): # either no 'scanid' was contained, or it was neither an int nor a castable representation of an int; e.g. a string
      pass


NONUSERCOLUMNS = [ 'timestamp', 'userid', 'preview', 'instrumentmetadata', 'instrumentmetadataschemaid', 'createdat', 'updatedat' ]
def insert(dbc, table, updateExisting=False, defaultScanIDformat='%07i', **kwargs):
    """upsert (insert/update) single rows in `table` with columns and values from `**kwargs`. Any non-existing columns will be aggregated in a JSON object under `metadata`.
    2023-11 jonas.graetz@fau.de"""
    
    availableColumns  = get_table_columns(dbc, table)
    primaryKeyColumns = get_table_primarykey(dbc, table)
    primaryKey = ', '.join(primaryKeyColumns)
    
    kwargs_keymap = dict(map(lambda s: (s.lower(), s), kwargs.keys()))
    for c in NONUSERCOLUMNS: kwargs.pop(kwargs_keymap.pop(c, None), None)
    if not all([ pk in kwargs_keymap for pk in primaryKeyColumns]):
        raise ValueError(f"column(s) {primaryKey} must be provided")
    
    # handle special case of scanID column:
    _format_scanid(kwargs, defaultScanIDformat=defaultScanIDformat)
    
    userMetadata = { k:kwargs[k] for k in kwargs.keys() if k.lower() not in availableColumns }
    if len(userMetadata) == 0: userMetadata = None
    
    metadata = None # initialize variable
    if userMetadata is not None:
        assert 'metadata' not in kwargs
        metadata=json.dumps(userMetadata, sort_keys=True, indent=2, default=defaultTypeConverter)
    
    providedColumnsList = [ col for col in kwargs.keys() if col.lower() in availableColumns ] # all other kwargs are already summarized in userMetadata
    
    insertColumnsString = ', '.join(providedColumnsList + [ 'metadata' ] if userMetadata is not None else [] )
    #special treatment of "timestamp" in order to allow for both text and unix epoch reprentations:
    insertValuesString  = ', '.join([ ('%s' if not (col == 'timestamp' and type(kwargs[col]) != str) else 'to_timestamp(%s)::timestamp')
                                        for col in providedColumnsList ] + [ '%s' ] if userMetadata is not None else [])
    updateColumnsString = (', '.join([ f"{col}=%s" for col in providedColumnsList if col.lower() not in ([ 'timestamp' ] + primaryKeyColumns) ]) \
                          + f", metadata=coalesce(({table}.metadata::jsonb || %s::jsonb)::json, %s)" if userMetadata is not None else ''
                          ).lstrip(', ') # remove a leading comma. Occurs if "metadata" is the only available column.
    
    if updateExisting:
      dbc.execute(f"""INSERT INTO {table}({insertColumnsString})
                      VALUES({insertValuesString})
                      ON CONFLICT ({primaryKey}) DO UPDATE
                      SET {updateColumnsString};""",
                        [kwargs[col] for col in providedColumnsList] + ([metadata] if userMetadata is not None else [ ])  \
                      + [kwargs[col] for col in providedColumnsList if col.lower() not in ([ 'timestamp' ] + primaryKeyColumns) ] \
                      + [metadata]*2 if userMetadata is not None else [  ])
    else:
      dbc.execute(f"""INSERT INTO {table}({insertColumnsString})
                      VALUES({insertValuesString});""", 
                        [kwargs[col] for col in providedColumnsList] + ([metadata] if userMetadata is not None else [ ]))


def insert_dataframe(dbc, table, dataframe, updateExisting=False, defaultScanIDformat='%07i', **kwargs):
    for index, row in dataframe.iterrows():
      insert(dbc, table, updateExisting=updateExisting, defaultScanIDformat=defaultScanIDformat, **(dict(row.dropna()) | kwargs))


def insert_spreadsheet(dbc, table, filename, updateExisting=False, defaultScanIDformat='%07i', **kwargs):
    df = pd.read_excel(filename)
    insert_dataframe(dbc, table, df, updateExisting=updateExisting, defaultScanIDformat=defaultScanIDformat, **kwargs)


def connect():
    """establish database connection and return cursor."""
    dbp = pg.connect(database, host,
                     port="5432", 
                     user=getpass("user:"), 
                     password=getpass("password:"))
    dbp.set_session(autocommit=True)
    dbpc = dbp.cursor()
    return dbpc


def get_table_columns(dbc, table):
    try:
      schema, table = table.split('.', 1)
    except ValueError: # no '.' in string, i.e., use 'public' schema
      schema = 'public'
    dbc.execute("SELECT column_name FROM information_schema.columns WHERE table_schema = %s AND table_name = %s;", (schema, table))
    return [ row[0] for row in dbc.fetchall() ] #output always in lowercase, no deed for explicit transform.


def get_table_primarykey(dbc, table):
    #https://wiki.postgresql.org/wiki/Retrieve_primary_key_columns
    dbc.execute("""
      SELECT a.attname
        FROM   pg_index i
        JOIN   pg_attribute a ON a.attrelid = i.indrelid
                        AND a.attnum = ANY(i.indkey)
        WHERE  i.indrelid = %s::regclass
        AND    i.indisprimary;
      """, (table,))
    return [ row[0] for row in dbc.fetchall() ]

     
class Catacore(object):
  def __init__(self):
    self.dbc = connect()
  def ingest(self, instrumentID, scanID, defaultScanIDformat='%07i', **kwargs):
    ingest(self.dbc, instrumentID, scanID, **kwargs)
  def ingest_hdf5(self, hdf5file, instrumentID, scanID, skipKeyList=["data"], **kwargs):
    ingest_hdf5(self.dbc, instrumentID, scanID, **kwargs)
  def insert(self, table, updateExisting=False, defaultScanIDformat='%07i', **kwargs):
    insert(self.dbc, table, updateExisting=updateExisting, defaultScanIDformat=defaultScanIDformat, **kwargs)
  def insert_dataframe(self, table, dataframe, updateExisting=False, defaultScanIDformat='%07i', **kwargs):
    insert_dataframe(self.dbc, table, dataframe, updateExisting=updateExisting, defaultScanIDformat=defaultScanIDformat, **kwargs)
  def insert_spreadsheet(self, table, filename, updateExisting=False, defaultScanIDformat='%07i', **kwargs):
    insert_spreadsheet(self.dbc, table, filename, updateExisting=updateExisting, defaultScanIDformat=defaultScanIDformat, **kwargs)
  
