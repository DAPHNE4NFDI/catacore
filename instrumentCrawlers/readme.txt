2023-12 jonas.graetz@fau.de

the files need to be placed together with the respective instrument's NeXusHDF5converter and the catactore module from "ingestion"
the catacore database connector will, by default, ask for username and password in interactive mode.
hardcoded credentials for a dedicated restricted catacore ingestor user may be added individually on deployment at the instrument
(cf. connect() call and definition in ingestion/catacore/__init__.py)

