#!/usr/bin/python3

#adapted from vaxsterpgmonitor. Currently converts ras files and assigns scanIDs based on internal database table lookup (list of all avaialble instrument files); TODO: update handle() and ingest

OLD_THRESH_SECONDS = 30 
LOOP_INTERVAL_SECONDS = 10

import re
import os
import sys
import json


from smartlab2nexus import convert_ras_file
from fileMonitor import fileMonitorLoop
from catacore import connect, ingest, rsm_lookup_scanID
from pyscicat.hdf5.scientific_metadata import scientific_metadata # takes an hdf5 file path and returns a python dictionary
import h5py
from matplotlib.pyplot import imsave
from matplotlib import cm
from numpy import array, clip, log1p, percentile, nan_to_num, average, inf
from io import BytesIO
import psycopg2 as pg

#refactored from 2023-01-14-VAXSTER-NeXus-SQLite.ipynb "def findEntries(folder)"
def readEntry(file):
    scanID = os.path.basename(file)[:7]   # use basename to remove heading path information from filename
    instrumentMetaData = scientific_metadata(file, skipKeyList = ["data"])
    return scanID, instrumentMetaData
    
def updateTable(db, vaxsterDataset, modifiedParameters=None, preview=None):
    scanID, metaDict   = vaxsterDataset
    modifiedParameters = None if modifiedParameters is None else json.dumps(modifiedParameters)
    instrumentMetadata = json.dumps(metaDict,indent=2)
    try:    date       = metaDict["instrument"]["detector"]["stop_time"]["value"]
    except: date       = None
    
    ingest(db, instrumentID='VAXSTER', scanID=scanID, timestamp=date, preview=preview,
               instrumentMetadata=instrumentMetadata, modifiedParameters=modifiedParameters)

    



from pathlib import Path

if __name__ == "__main__":
    monitorPath  = sys.argv[1]
    h5outputPath = sys.argv[2]
    startnumber  = int(sys.argv[3])
    #excludeList  = [ "%s/fr_%07i_raw.zip" % (monitorPath, i) for i in range(80000, startnumber) ]
    #print(excludeList[-1], len(excludeList))
    #monitorGlobPattern = f"{monitorPath}/fr_[0-9][0-9][0-9][0-9][0-9][0-9][0-9]_raw.zip"
    
    filelist = list(Path(monitorPath).rglob('*.ras'))
    
    dbpc = connect()
       
    print('looking up scanids')
    filedict = { f:rsm_lookup_scanID(dbpc, f"./{str(f)}")[0] for f in filelist}
    print(f'found {len(filedict)} ids')
    print('converting..')
    for f, scanid in filedict.items():
      if int(scanid.rstrip('abcdefghijklmnopqrstuvwxyz').lstrip('0')) >= startnumber:
        print(scanid, f)
        try:
          convert_ras_file(f, f"{h5outputPath}/{scanid}.h5")
        except:
          pass
    
    exit
    

    
    #handle = lambda file: print("write %s" % file) # debug dummy
    def handle(file):
        print("handle %s" % file)
        try:
          identifier = re.findall("[0-9]+", file)[-1]
          h5filename = VAXSTER2NeXus(file, h5outputPath, os.path.dirname(file) + '/../images/corrected/im_%s_caz.tiff' % identifier)
        except KeyboardInterrupt:
          dbpc.close()
          raise
        except:
            print("Error converting file")
            pass
        try:    thumbnail = getVaxster3Preview(h5filename)
        except: thumbnail = None
        try: 
          updateTable(dbpc, 
                      readEntry(h5filename), 
                      None if handle.prevH5File is None 
                           else vaxsterParameterDiff(readEntry(h5filename       )[1], 
                                                     readEntry(handle.prevH5File)[1]
                                                    ),
                      pg.Binary(thumbnail)
                      )
          handle.prevH5File = h5filename
        except KeyboardInterrupt:
          dbpc.close()
          raise
        except UnboundLocalError: # h5filename was undefined, because VAXSTER2NeXus() failed earlier. Skip file 
          handle.prevH5File = None;
        except:
          dbpc.close()
          raise
    handle.prevH5File = None;
    handle.prevH5File = f"{h5outputPath}/{startnumber-1:07}.h5"
    if not os.path.exists(handle.prevH5File):
      handle.prevH5File = None
    
      
    
    dbpc = connect()
    
    # start infinite loop:
    fileMonitorLoop(monitorGlobPattern, handle,
                    ageThresholdSeconds=OLD_THRESH_SECONDS,
                    loopIntervalSeconds=LOOP_INTERVAL_SECONDS,
                    excludeList=excludeList)

