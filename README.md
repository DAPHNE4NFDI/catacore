# Catacore: a lightweight asynchronous instrument meta-data aggregation and annotation system with sample & proposal database

For the catalogization of research data from scientific instruments within the DAPHNE4NFDI project, a suited pre-catalogization system for the enrichment of (automatically collected) instrument-metadata with essential user-provided metadata (i.e., description, userID, sampleID, proposal/experimentID, datasetID, user-defined meta-data fields, sample classification and reference to further electronic logs and PIDs) is required. Catacore fills this niche especially with simplicity of use and administration in a university research group environment in mind.

Catacore is a self-contained PostgreSQL application implementing an online multiuser meta-data aggregation and tagging solution, sample & proposal database, raw-data catalog and tabular Electronic Lab-Notebook (ELN) with flexible and user-extensible meta-data schemata and proposal-based access control. The minimal tech-stack & maximal use of widely adopted and supported tools make it perfectly suited for integration in standard IT infrastructures and identity management systems. Instrument (or simulation) meta-data can be automatically ingested e.g. from HDF5/NeXus files, and depending on the particular use-case, users may directly interact with the system analog to spreadsheets with common SQL GUIs or ingest meta-data from spreadsheets. Also, EuXFELs dedicated tagging tool DAMNIT is an ideal fit.

While the system provides the simplicity of basic spreadsheets to unexperienced users, advanced users can profit from the full potential of an SQL engine. All data safety and security is implemented in the SQL backend, ensuring that SQL queries need not be further filtered by a user frontend.

-- 2023-10 jonas.graetz@fau.de

    This work was supported by the consortium DAPHNE4NFDI in the context of the work of the NFDI e.V.  
    The consortium is funded by the DFG - project number 460248799



## Getting started

* use manged PostgreSQL instance of your IT center (preferred) or set up own instance
* get a SQL GUI, suggested: DBeaver (Desktop Application), CloudBeaver (Container-hosted WebUI)
* if not managed by IT center, consider role/identity management options: manual, operating system, LDAP, etc. (cf. PostgreSQL documentation)
* obtain/set up a dedicated user/role for the database owner/administrator; 
* obtain/set up a dedicated user/role for instrument ingestor scripts
* obtain/set up a dedicated database within your PostgreSQL instance
* connect to that database with administrator/owner role
* create catacore structure within the given postgresql-database using src/*.sql
* to grant users to your catacore instance, add respective postgresql roles to sys.users table
* add ingestors to your catacore instance, add respective postgresql roles to sys.ingestors table
* connect with user or ingestor role:
    - start ingesting / adding data to tables!

