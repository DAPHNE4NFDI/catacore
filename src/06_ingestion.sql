/* Manages automated ingestions with mininimal (insert only) access rights to the database.
 * 
 * 2023 Jonas Graetz <jonas.graetz@fau.de>
 * 
 * I.e., no reading and no modification of already provided meta-data fields allowed.
 * Significantly minimizes implications of compromised ingestor-script credentials.
 * Realized by means of an "instead of insert" trigger on the dedicated view sys.scanlog_ingestion_stage.
 * 
 * requires: 
 * base.sql: eln.chk_user_esist(), tables in schema eln
 * rights.sql: table sys.users
 * archive.sql: eln.chk_proposalid_is_frozen_before_insert_or_update() 
 * wizards.sql (recommended): to ensure silent handling of not-yet existing sample and proposal entries */


drop view if exists sys.scanlog_ingestion_stage cascade;
/* ensure concistency with eln.scanlog table. On changes, update also the UPDATE statement in function sys.trigger_scanlog_ingestion_handler() */
create view sys.scanlog_ingestion_stage as 
select
	localtimestamp(0)  as "timestamp",
    null::varchar      as userID,
    null::varchar      as instrumentID,
    null::varchar      as scanID,
    null::varchar      as sampleID,
    null::varchar      as description,
    null::varchar      as proposalID,
    null::varchar      as datasetID,
    null::bytea        as preview,
    null::json         as modifiedParameters, 
    null::json         as metadata,
    null::json         as instrumentMetadata,
    null::varchar      as metadataSchemaID,
    null::varchar      as instrumentMetadataSchemaID ,
    null::varchar      as extReference;
    
create or replace function sys.trigger_scanlog_ingestion_handler() returns trigger as $tf$ begin
	/* instead of insert -- use for rights escalation to database owner ("security definer"),
	 *  so that the ingestor does not require any permissions on eln and rel. 
	 * as instead triggers are only possible on views, apply this to a dedicated dummy view*/
	if    new.instrumentMetadata is null then 
        raise exception 'instrumentMetadata must not be NULL';
    elsif new.instrumentID is null or new.scanID is null then
        raise exception 'instrumentID and scanID must not be NULL';
    end if;
 
   /* validate userID and attempt to interpret as / map from email-address if not exists. If it also is not a known email address, the result will be NULL */
    if (new.userID is not null) and not eln.chk_user_exists(new.userID) then 
	    new.userID := (select idmID from sys.users where email=new.userID); -- "sys.users.email" is defined primary key / unique, i.e., the statement will produce exactly one userID or NULL
    end if;
   
    /* silently drop a frozen/inappropriate proposalID in order to not interrupt the automated ingestion. The user will add the information on manual review*/
    if (new.proposalID is not null) and eln.chk_proposalid_is_frozen_before_insert_or_update(new.proposalID) then
        new.proposalID := null; 
    end if;
   
    insert into eln.scanlog("timestamp", userID,      instrumentID,     scanID,     sampleID,     description,     proposalID,     datasetID,     preview,     modifiedParameters,     metadata,     instrumentMetadata,     metadataSchemaID,     instrumentMetadataSchemaID,     extReference)
    values (coalesce(new.timestamp, localtimestamp(0)), 
                                      new.userID, new.instrumentID, new.scanID, new.sampleID, new.description, new.proposalID, new.datasetID, new.preview, new.modifiedParameters, new.metadata, new.instrumentMetadata, new.metadataSchemaID, new.instrumentMetadataSchemaID, new.extReference)
    on conflict (instrumentID, scanID) do update set
       "timestamp"  = coalesce(new.timestamp          , localtimestamp(0))    ,  -- coalesce: returns first non-null value
       userID       = coalesce(eln.scanlog.userID     , new.userID)           ,  -- prioritize user-provided entries if already present
       sampleID     = coalesce(eln.scanlog.sampleID   , new.sampleID)         ,
       description  = coalesce(eln.scanlog.description, new.description)      ,
       proposalID   = coalesce(eln.scanlog.proposalID , new.proposalID)       ,
       datasetID    = coalesce(eln.scanlog.datasetID  , new.datasetID)        ,
       preview      = coalesce(eln.scanlog.preview    , new.preview)          ,
       modifiedParameters =                             new.modifiedParameters,
       metadata     = coalesce(eln.scanlog.metadata   , new.metadata)         ,
       instrumentMetadata =                             new.instrumentMetadata,  -- the following WHERE clause (silently/without error) prohibits overwriting existing instrumentMetadata. Further, trigger "chk_protect_instrument_entries_before_update" on eln.scanlog also protects existing instrumentMetadata
       metadataSchemaID = coalesce(eln.scanlog.metadataSchemaID, new.metadataSchemaID),
       instrumentMetadataSchemaID = coalesce(eln.scanlog.instrumentMetadataSchemaID, new.instrumentMetadataSchemaID),
       extReference  = coalesce(eln.scanlog.extReference, new.extReference),
       updatedAt     = localtimestamp(0)
    where eln.scanlog.instrumentMetadata is null; -- do nothing if the entry has previously been updated by the instrument ingestor / an instrument. This simplifies re-running ingestion scripts; may however leave errors or ineffective intentional overwrite attempts unnoticed.
    --TODO: raise an error / warning when attempting to overwrite existing entries / to give uesr-feedback on the occasion of e.g. duplicate scanIDs etc.
    --TODO: this could be achieved by removing the where clause and leave the protection to the "before update" trigger on eln.scanlog

return null; end; $tf$ language plpgsql security definer;

drop trigger if exists scanlog_ingestion_handler on sys.scanlog_ingestion_stage;
create trigger scanlog_ingestion_handler instead of insert on sys.scanlog_ingestion_stage for each row execute function sys.trigger_scanlog_ingestion_handler();
