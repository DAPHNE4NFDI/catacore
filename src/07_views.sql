/* convenience views of eln.scanlog with additional, usecase specific meta-data columns for individual instruments (e.g. SAXS, GISAXS, XRD, AFM, etc.) 
 * https://www.postgresql.org/docs/current/sql-createview.html
 * NOTE: views are, up to postgreSQL 15, always associated with the rights of the owner (not the invoking user). 
 * NOTE: this implies that also INSERT/UPDATE/DELETES would be executed on the backend tables with table owner/administrator rights.
 * NOTE: when enabling editing (by granting INSERT/UPDATE/DELETE), "instead of" triggers MUST THUS be installed in order to 
 *       prevent an implicit rights escalation from current_user to db_owner (view owner). (the triggers will be executed in the current_user context)
 */


--https://www.postgresql.org/docs/current/sql-createview.html
---SELECT version();

create schema vws; 
ALTER DEFAULT PRIVILEGES IN SCHEMA vws GRANT SELECT ON TABLES TO public; 
-- adding also insert/update/delete rights is done on a per-view basis, may only be granted if a modification_handler trigger has been enabled to prevent inadvertent rights escalation

/* NOTE: setting up "instead of insert/update/delete" triggers (cf. vws.trigger_instrumentview_modification_handler()) is OBLIGATORY for all views,
 *  as these commands will otherwise be executed with owner/administrator rights */

/* this handler will be called in user context and thus perform updates on the source tables of the view with rights of current_user as opposed to view owner / db administrator.
 * it is used for all instrument-specific views of eln.scanlog, the respective instrumentID is taken from the trigger arguments array TG_ARGV[]. 
 * it is assumed that the all views generally contain at least all the user-editable columns of eln.scanlog. Any additional informative/"computed" columns are ignored for the purpose of updates*/
create or replace function vws.trigger_instrumentview_modification_handler() returns trigger as $tf$ begin
	/* parameters: TG_ARGV[0] : instrumentID) */
    if (tg_op = 'UPDATE') then 
        update eln.scanlog set
            userID     = case when (new.userID is distinct from old.userID) then new.userID else coalesce(new.userID, current_user) end,  /* ensures a "default current_user if cell is left NULL", while also allowing to explicitly reset of userID to NULL */
            proposalID = new.proposalID,
            sampleID   = new.sampleID,
            datasetID  = new.datasetID,
            description= new.description,
            metadata   = new.metadata,
            metadataSchemaID = new.metadataSchemaID,
            extreference=new.extreference
        where (eln.scanlog.instrumentID, eln.scanlog.scanID) = (TG_ARGV[0], old.scanID);
        if (new.scanID is distinct from old.scanID) then /* assuming that scanID is seldomly corrected (only for user-generated entries without instrumentMetadata), put this into a dedicated statement. Avoids related triggers for the typical case of old.scanID = new.scanID */
            update eln.scanlog set 
                scanID=new.scanID
            where (eln.scanlog.instrumentID, eln.scanlog.scanID) = (TG_ARGV[0], old.scanID);
        end if;
	elsif (tg_op = 'INSERT') then
        insert into eln.scanlog(instrumentID, scanID, userID, proposalID, sampleID, datasetID, description, metadata, metadataschemaid, extreference) 
        values (TG_ARGV[0], new.scanID, current_user, new.proposalID, new.sampleID, new.datasetID, new.description, new.metadata, new.metadataschemaid, new.extreference);
    elsif (tg_op = 'DELETE') then
        delete from eln.scanlog where (instrumentID, scanID) = (TG_ARGV[0], old.scanID);
    end if;
return null; end;
$tf$ language plpgsql;




drop trigger if exists modification_handler on vws.testview;
drop view vws.testview;
create view vws.testview with (security_barrier) as select 
    eln.scanlog."timestamp",
	eln.scanlog.userID,
    proposalID,
	--eln.proposals.title,
	datasetID,
	--instrumentID, 
	scanID,
	sampleID,
	eln.scanlog.description,
	eln.scanlog.metadata,
	modifiedParameters,
	instrumentMetaData,
	eln.scanlog.metadataSchemaID,
	extreference
from eln.scanlog where instrumentID='TEST'
with check option; /* limits editable rows to visible rows in the absence of an INSTEAD OF trigger */
create trigger modification_handler instead of insert or update or delete on vws.testview for each row execute function vws.trigger_instrumentview_modification_handler('TEST');
grant INSERT, UPDATE, DELETE ON vws.testview to public;


drop trigger if exists modification_handler on vws.vaxster_saxs;
drop view if exists vws.vaxster_saxs;
create or replace view vws.vaxster_saxs with (security_barrier) as -- "create view vws.vaxster_saxs with (security_invoker = true) as" only supported from postgresql v15 on; use explicit row policy rules here:
select 
	eln.scanlog."timestamp",
	proposalID,
	--eln.proposals.title,
	eln.scanlog.userID,
	datasetID,
	scanID,
	sampleID,
	eln.scanlog.description,
	instrumentmetadata #>> '{instrument,positioners,att       ,value_set, value}' as att,
	instrumentmetadata #>> '{instrument,positioners,bstop     ,value_set, value}' as bstop,
	instrumentmetadata #>> '{instrument,detector   ,count_time,value}'            as count_time,
	instrumentmetadata #>> '{instrument,positioners,detx      ,value_set, value}' as detx,
	instrumentmetadata #>> '{instrument,positioners,zkstage   ,value_set, value}' as zkstage,
	instrumentmetadata #>> '{instrument,positioners,ystage    ,value_set, value}' as ystage,
	eln.scanlog.metadata,
	modifiedParameters,
	instrumentMetaData,
	eln.scanlog.metadataSchemaID,
	eln.scanlog.extreference
from eln.scanlog --join eln.proposals using (proposalID) 
where
	instrumentID='VAXSTER' and
	(eln.scanlog.userid in (current_user, 'public') or eln.scanlog.userID is null or exists
		(select 1 
		from rel.userproposals
		where (rel.userproposals.userID, rel.userproposals.proposalID) = (eln.scanlog.userID, eln.scanlog.proposalID)))
order by scanID desc
with check option; /* limits editable rows to visible rows in the absence of an INSTEAD OF trigger */
create trigger modification_handler instead of insert or update or delete on vws.vaxster_saxs for each row execute function vws.trigger_instrumentview_modification_handler('VAXSTER');
grant INSERT, UPDATE, DELETE ON vws.vaxster_saxs to public;

drop trigger if exists modification_handler on vws.vaxster_gisaxs;
drop view if exists vws.vaxster_gisaxs;
create or replace view vws.vaxster_gisaxs with (security_barrier) as
select 
	"timestamp",
	proposalID,
	userID,
	datasetID,
	scanID,
	sampleID,
	description,
	instrumentmetadata #>> '{instrument,positioners,att,value_set, value}'     as att,
	instrumentmetadata #>> '{instrument,positioners,bstop,value_set, value}'   as bstop,
	instrumentmetadata #>> '{instrument,detector,count_time, value}'           as count_time,
	instrumentmetadata #>> '{instrument,positioners,detx,value_set, value}'    as detx,
	instrumentmetadata #>> '{instrument,positioners,zkstage,value_set, value}' as zgstage,
	instrumentmetadata #>> '{instrument,positioners,ystage,value_set, value}'  as ystage,
	instrumentmetadata #>> '{instrument,positioners,cstage,value_set, value}'  as cstage,
	instrumentmetadata #>> '{instrument,positioners,pstage,value_set, value}'  as pstage,
	metadata,
	modifiedParameters,
	instrumentMetaData,
	eln.scanlog.metadataSchemaID,
	eln.scanlog.extreference
from eln.scanlog 
where
	instrumentID='VAXSTER' and
	(userid in (current_user, 'public') or eln.scanlog.userID is null or exists
		(select 1 
		from rel.userproposals
		where (rel.userproposals.userID, rel.userproposals.proposalID) = (eln.scanlog.userID, eln.scanlog.proposalID)))
order by scanID desc
with check option; /* limits editable rows to visible rows in the absence of an INSTEAD OF trigger */
create trigger modification_handler instead of insert or update or delete on vws.vaxster_gisaxs for each row execute function vws.trigger_instrumentview_modification_handler('VAXSTER');
grant INSERT, UPDATE, DELETE ON vws.vaxster_gisaxs to public;

drop trigger if exists modification_handler on vws.rigaku_smartlab;
drop view if exists vws.rigaku_smartlab;
create view vws.rigaku_smartlab  with (security_barrier)  as /* security_barrier */
select 
	"timestamp",
	userID,
	proposalID,
	datasetID,
	scanID,
	sampleID,
	description,
	(instrumentmetadata #>> '{duration, value}'                       )::int     as duration,
	(instrumentmetadata #>> '{instrument,source,monochromator, value}')::text    as monochromator,
	(instrumentmetadata #>> '{instrument,axes,ThetaS,value}'          )::float   as theta_S,
	(instrumentmetadata #>> '{instrument,axes,CBO, value}'            )::text    as CBO,
	(instrumentmetadata #>> '{instrument,axes,ReceivingOptics, value}')::text    as receivingOptics,
	metadata,
	modifiedParameters,
	instrumentMetaData,
	eln.scanlog.metadataSchemaID,
	eln.scanlog.extreference
from eln.scanlog 
where
	instrumentID='SMARTLAB' and
	(userid in (current_user, 'public') or userid is null or exists
		(select 1 
		from rel.userproposals
		where (rel.userproposals.userID, rel.userproposals.proposalID) = (eln.scanlog.userID, eln.scanlog.proposalID)))
order by scanID desc
with check option; /* limits editable rows to visible rows in the absence of an INSTEAD OF trigger */
create trigger modification_handler instead of insert or update or delete on vws.rigaku_smartlab for each row execute function vws.trigger_instrumentview_modification_handler('SMARTLAB');
grant INSERT, UPDATE, DELETE ON vws.rigaku_smartlab to public;



