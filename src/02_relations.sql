/* relational tables for the purpose of rights management and convenient joins 
 * 
 * 2023, Jonas Graetz <jonas.graetz@fau.de>
 * 
 * the tables summarize user/proposal/sample/project relations directly or indirectly established in:
 *    eln.scanlog   (where many-to-many proposalID - sampleID relations are established)
 *    eln.proposals (where one-to-many  proposalID - userID   relations are established)
 *    eln.samples   (where (owner) userID - sampleID relations are established)
 *    eln.projects  (where (owner) userID - projectID relations are established)
 * 
 *   the following relational tables are built: 
 *    rel.userproposals    (generated solely from eln.proposals(userID, accessUserIDs))
 *    rel.proposalsamples  (generated solely from eln.scanlog(proposalID, sampleID))
 *    rel.projectproposals (generated solely from eln.proposals(proposalID, projectID))  -- consider following parent[Proposal|Project]IDs here as well? -- NO: the parent..ID concept is reserved for a somewhat userdefined interpretation of what that hierarchy means and implies exactly.  
 *    rel.usersamples      (represents a join of rel.userproposals and rel.proposalsamples  on proposalID + all direct eln.samples (userID,sampleID)  relations)
 *    rel.userprojects     (represents a join of rel.userproposals and rel.projectproposals on proposalID + all direct eln.projects(userID,projectID) relations)
 * 
 * for performance reasons all relational tables are modified selectively on updates to the tables in eln (as opposed to being implemented as views, or as opposed to being entirely re-built on any operation)
 * 
 * for clarity & user transparency, no further indirections are made. 
 * One example would be: join userprojects, projectproposals on projectID in order to infer implicit userID-proposalID relations through the eln.projects table. 
 * This (plausible) usecase is captured by an explicit wizward adding, by default, the owner of (the userID of) the referred projectID to the accessUserIDs list in eln.proposals.
 * 
 * When dropping and rebuilding the tables (e.g., because the foreign key constraints were dropped after deletion and recreation of the eln tables),
 * then all triggers (defined here) AND ALSO policies (defined in rights.sql) on the eln.* tables must be reinstated again. 
 *
 */

create schema rel;

drop table if exists rel.usersamples cascade;
create unlogged table if not exists rel.usersamples(
   userID varchar not null,
   sampleID varchar not null references eln.samples(sampleID) on update cascade on delete cascade,
   primary key(userID, sampleID)
   );
create index usersamples_sampleID_idx on rel.usersamples(sampleID);

-- prepare access rights enforcement by synching authorized users from the eln.proposals table to a more efficient (internal) relational table.
drop table if exists rel.userproposals cascade;
create unlogged table rel.userproposals(
   proposalID varchar not null references eln.proposals(proposalID) on update cascade on delete cascade,
   userID varchar not null, 
   primary key (proposalID, userID)
  );                 /* fast lookups for proposalID, required for efficient updates to the table */
  create index userproposals_userID_idx on rel.userproposals (userID); /* fast loopups for userID, required for efficient row-level security */
-- note: as a general design decision, updates and deletions of proposalIDs should be prohibited (at least once they are referenced by scans)
-- nevertheless, ensure consistency here and specify cascades


drop table if exists rel.proposalsamples cascade;
create unlogged table rel.proposalsamples(
   proposalID varchar not null references eln.proposals(proposalID) on update cascade on delete cascade,
   sampleID   varchar not null references eln.samples(sampleID)     on update cascade on delete cascade,
   refcount   integer default 1,
   primary key (proposalID, sampleID)
   ); create index proposalsamples_sampleID_idx on rel.proposalsamples(sampleID);

drop table if exists rel.userprojects cascade;
create unlogged table rel.userprojects (
   userID    varchar not null,
   projectID varchar not null references eln.projects(projectID) on update cascade on delete cascade, 
   primary key (userID, projectID)
  ); create index userprojects_projectID_idx on rel.userprojects (projectID);


drop table if exists rel.projectproposals cascade;
create unlogged table rel.projectproposals (
   projectID  varchar not null references eln.projects(projectID)   on update cascade on delete cascade,
   proposalID varchar not null references eln.proposals(proposalID) on update cascade on delete cascade, 
   --refcount  int default 1,  -- the relation can only be 1:1 by design of the tables
   primary key (projectID, proposalID)
);   
create index projectproposals_proposalID_idx on rel.projectproposals (proposalID);

create or replace function rel.trigger_update_userproposals_after_modificaton_on_proposals() returns trigger as $$ begin
	/*create trigger sys_update_userproposals_after_modification   after insert or delete or update of accessUserIDs, userID on eln.proposals for each row execute function rel.trigger_update_userproposals_after_insert_or_update_on_proposal();*/
    /* new.proposalID is PK / has a not null constraint (in the eln.proposals table); thus no explicit check for IS NOT NULL is required */
    /* we only need to consider changes in accessUserIDs, as changes/deletion to/of proposalID are handled by the foreign key constriant*/
	/* in order to avoid potential concurrency issues in usersamples, which checks also userproposals, delete only selectively those userIDs that have actually been removed, even though they would be added lateron again anyway:*/
    if (tg_op = 'UPDATE') then delete from rel.userproposals where rel.userproposals.proposalid = new.proposalID and rel.userproposals.userid != all(new.accessUserIDs) and rel.userproposals.userid != new.userID; end if;  /* use new.proposalID, as potential changes are already cascaded through the foreign key relations at this stage*/
    -- DELETE captured already by "on delete cascade" FK constraint: --elsif (tg_op = 'DELETE') then delete from rel.userproposals where rel.userproposals.proposalid = old.proposalID; end if;
	/* ensure proposal author/contact is always added: */
    if tg_op in ('INSERT', 'UPDATE') then
        insert into rel.userproposals (proposalID, userID)
            select new.proposalID as pid, new.userID as uid where new.userID is not null 
            union all
            select new.proposalID as pid, uid from lateral unnest(new.accessUserIDs) as uid where uid is not null -- capture corner case of NULL's in the accessUserIDs, would not be captured by "on conflict". use lateral join as otherwise WHERE cannot refer to uid
            on conflict do nothing; -- in case usernames are listed redundantly
    end if;
return null; end; $$ language plpgsql security definer;

/* updates and deletes of eln.proposals(proposalID) will be handled by a foreign key constraint */


/* synchronize proposal-sample relations established through user-provided annotations in the eln.scanlog table. */
/* cf. also rel.trigger_correct_refcount_after_FK_update_on_proposalsamples() */
create or replace function rel.trigger_update_proposalsamples_after_modification_on_scanlog() returns trigger as $$ begin
    /*create trigger sys_update_proposalsamples after insert or delete or update of proposalID, sampleID  on eln.scanlog   for each row execute function rel.trigger_update_proposalsamples_after_insert_or_update();*/
	/* check before insert: */
    if tg_op in ('INSERT', 'UPDATE') and (new.proposalID is not null and new.sampleID is not null) then 
        insert into rel.proposalsamples (proposalID, sampleID) values (new.proposalID, new.sampleID)
        on conflict (proposalID, sampleID) do update set refcount=rel.proposalsamples.refcount+1; 
        /* in case proposalID, sampleID didn't actually change value during an UPDATE operation, the refcount will be correctly reduced again in the next operation */
    end if;
	/* no explicit check required for null on update/delete: */
    /* the following branch will have no effect in the case of foreign-key induced cascaded update, as (old.proposalID, old.sampleID) entries will have been updated already in rel.proposalsamples / do not exist anymore. 
     * cf. rel.trigger_correct_refcount_after_FK_update_on_proposalsamples() */
    if tg_op in ('UPDATE', 'DELETE') then        
        /* no explicit check for NULL on old.sampleID and old.proposalID required. where-clause whill not match NULL entries*/
		update rel.proposalsamples set refcount=rel.proposalsamples.refcount-1 where (rel.proposalsamples.proposalID, rel.proposalsamples.sampleID) = (old.proposalID, old.sampleID);
	    /* performing the delete after the update and testing for refcount<1 (as opposed to refcount=0, and instead of before the update, with refcount=1)
         *  ensures this trigger function to remain also valid when executed concurrently */
        delete from rel.proposalsamples where (proposalID, sampleID) = (old.proposalID, old.sampleID) and refcount < 1;
	end if;
    return null;
end; $$ language plpgsql security definer;


/* Catch the case of "sampleid" or "proposalid" being updated in eln.samples or eln.proposals, as
 * this update will cascade both to rel.proposalsamples *and* eln.scanlog, triggering also rel.trigger_update_proposalsamples_after_modification_on_scanlog().
 * rel.trigger_update_proposalsamples_after_modification_on_scanlog() cannot detect this special case and will erroneously increase the refcount without also decreasing it correctly. 
 * As changes of foreign keys are the only action triggering an UPDATE (as opposed to INSERT/DELETE) on columns rel.proposalsamples(proposalID, sampleID), 
 * the refcount can be explicitly reduced again in this case, relying on the fact that it has been or will be increased by update_proposalsamples_after_modification_on_scanlog.*/
create or replace function rel.trigger_correct_refcount_after_FK_update_on_proposalsamples() returns trigger as $$ begin
	/*complementing rel.trigger_update_proposalsamples_after_modification_on_scanlog() */
	if (new.proposalID, new.sampleID) != (old.proposalID, old.sampleID) then -- must be ensured, otherwise rel.trigger_update_proposalsamples_after_modification_on_scanlog() did already successfully decrease refcount.
		update rel.proposalsamples set refcount=rel.proposalsamples.refcount-1 where (rel.proposalsamples.proposalID, rel.proposalsamples.sampleID) = (new.proposalID, new.sampleID);
	    /* performing the delete after the update and testing for refcount<1 (as opposed to refcount=0, and instead of before the update, with refcount=1)
	     *  ensures this trigger function to remain also valid when executed concurrently (with this or other ones) */
	    delete from rel.proposalsamples where (proposalID, sampleID) = (old.proposalID, old.sampleID) and refcount < 1;
	end if;
    return null;
end; $$ language plpgsql security definer;


create or replace function rel.trigger_update_projectproposals_after_modification_on_proposals() returns trigger as $$ begin
	/*create trigger sys_update_projectproposals_after_insert_or_update after insert or update of projectID             on eln.proposals for each row execute function rel.trigger_update_projectproposals_after_insert_or_update(); */
	/*trigger after insert or update of projectID on eln.proposals */
	if    (tg_op = 'UPDATE') then delete from rel.projectproposals where (projectID, proposalID) = (old.projectID, new.proposalID); end if; /* update of projectID,  no explicit check for null necessary for delete: */
	    /* potential changes of proposalID are captured by "on cascade" foreign key relations. use "new.proposalID", as any changes would have already cascaded through all tables*/
	    /* in case the UPDATE was triggered by an FK cascade on projectID, "old.projectID" might not exist anymore in the rel.projectproposals table, as it might already be updated to the new.projectID automatically. This is OK.*/
	-- DELETE captured already by foreign key "on delete cascade": --elsif (tg_op = 'DELETE') then delete from rel.projectproposals where (projectID, proposalID) = (old.projectID, old.proposalID); end if;
    if tg_op in ('INSERT', 'UPDATE') and (new.projectID is not null and new.proposalID is not null) then -- "is null" would not be captured by "on conflict do nothing", so test explicitly
		insert into rel.projectproposals(projectID, proposalID) values (new.projectID, new.proposalID) on conflict do nothing; 
		/* conflicts can occur on UPDATE when the projectID has already been updated (when the UPDATE was triggered) through an FK cascade on ProjectID as opposed by explicit user action */
	end if;
return null; end; $$ language plpgsql security definer;

create or replace function rel.trigger_update_usersamples_before_delete_on_userproposals() returns trigger as $$ begin
	/*create trigger sys_update_usersamples_before_delete    before delete    on rel.userproposals   for each row execute function rel.trigger_update_usersamples_before_delete_on_userproposals();*/
-- execute BEFORE delete of userproposals, so that the "old" tables are still there
-- update does not need to be considered: they only happen to modify refcount, or for automatically cascaded modifications of FK proposalID
-- to count the "paths" from a user to a sample, and delete the (userID, sampleID) if the old.proposalID was the only connection.
-- this procedure is meaningful for the case of a modified 
delete from rel.usersamples where (userID, sampleID) in 
    (select
         userID,
         sampleID
     from rel.userproposals join rel.proposalsamples using (proposalID)
     where  userID = old.userID
     group by (userID, sampleID)
     having
         count(proposalID) filter (where proposalID != old.proposalID) = 0  -- there are no other proposals relating this user with this sample
         and not exists (select 1 from eln.samples where (eln.samples.sampleID, eln.samples.userID)=(rel.proposalsamples.sampleID, rel.userproposals.userID)) -- and the user is not the owner of the sample
     );
return old; end; $$ language plpgsql security definer;


create or replace function rel.trigger_update_usersamples_before_delete_on_proposalsamples() returns trigger as $$ begin
	/*create trigger sys_update_usersamples_before_delete    before delete    on rel.proposalsamples for each row execute function rel.trigger_update_usersamples_before_delete_on_proposalsamples();*/
-- execute BEFORE delete of proposalsamples, so that the "old" tables are still there
-- updates doe not need to be considered: they only happen to modify refcount, or for automatically cascaded modifications of FK proposalID
-- to count the "paths" from a user to a sample, and delete the (userID, sampleID) if the old.proposalID was the only connection.
-- this procedure is meaningful for the case of a modified 
delete from rel.usersamples where (userID, sampleID) in 
    (select
         userID,
         sampleID
     from rel.userproposals join rel.proposalsamples using (proposalID)
     where  sampleID = old.sampleID
     group by (userID, sampleID)
     having
         count(proposalID) filter (where proposalID != old.proposalID) = 0  -- there are no other proposals relating this user with this sample
         and not exists (select 1 from eln.samples where (eln.samples.sampleID, eln.samples.userID)=(rel.proposalsamples.sampleID, rel.userproposals.userID)) -- and the user is not the owner of the sample
     );     
return old; end; $$ language plpgsql security definer;


create or replace function rel.trigger_update_userprojects_before_delete_on_projectproposals() returns trigger as $$ begin
	/*create trigger sys_update_userprojects_before_delete   before delete    on rel.projectproposals    for each row execute function rel.trigger_update_userprojects_before_delete_on_projectproposals();*/
-- execute BEFORE delete of projectproposals, so that the "old" tables are still there // potentially the trigger would always see the old table, cf. "transaction snapshots"
-- to count the "paths" from a user to a project, and delete the (userID, projectID) if the old.proposalID was the only connection.
delete from rel.userprojects where (userID, projectID) in 
    (select
         userID,
         projectID
     from rel.userproposals join rel.projectproposals using (proposalID)
     where  projectID = old.projectID
     group by (userID, projectID)
     having
         count(proposalID) filter (where proposalID != old.proposalID) = 0  -- there are no other proposals relating this user with this project
         and not exists (select 1 from eln.projects where (eln.projects.projectID, eln.projects.userID)=(rel.projectproposals.projectID, rel.userproposals.userID)) -- and the user is not the owner of the project
     ); 
return old; end; $$ language plpgsql security definer;


create or replace function rel.trigger_update_userprojects_before_delete_on_userproposals() returns trigger as $$ begin
	/*create trigger sys_update_userprojects_before_delete   before delete    on rel.userproposals       for each row execute function rel.trigger_update_userprojects_before_delete_on_userproposals();*/
-- execute BEFORE delete of userproposals, so that the "old" tables are still there // potentially the trigger would always see the old table, cf. "transaction snapshots"
-- to count the "paths" from a user to a project, and delete the (userID, projectID) if the old.proposalID was the only connection.
delete from rel.userprojects where (userID, projectID) in 
    (select
         userID,
         projectID
     from rel.userproposals join rel.projectproposals using (proposalID)
     where  userID = old.userID
     group by (userID, projectID)
     having
         count(proposalID) filter (where proposalID != old.proposalID) = 0  -- there are no other proposals relating this user with this project
         and not exists (select 1 from eln.projects where (eln.projects.projectID, eln.projects.userID)=(rel.projectproposals.projectID, rel.userproposals.userID)) -- and the user is not the owner of the project
     ); 
return old; end; $$ language plpgsql security definer;


create or replace function rel.trigger_update_usersamples_after_insert_or_update_on_samples() returns trigger as $$ begin
	/*create trigger sys_update_usersamples_after_insert_or_update_on_samples after insert or update of userID on eln.samples for each row execute function rel.trigger_update_usersamples_after_insert_or_update_on_samples();*/
	if (tg_op = 'UPDATE') and (new.userID is distinct from old.userID) then /* will also evaluate to true for the case of new.userID is null and old.userID is null; won't harm however.*/
	   /* in the case of a simultaneously updated sampleID, the FK cascade has already performed the renaming, so use new.sampleID explicitly. (confirmed by explicit test) */
        delete from rel.usersamples 
            where (rel.usersamples.userID, rel.usersamples.sampleID) = (old.userID, new.sampleID)  
            and not exists
               (select 1 from rel.userproposals join rel.proposalsamples using (proposalID)
                 where  (rel.userproposals.userID, rel.proposalsamples.sampleID) = (old.userID, new.sampleID));
	end if; 
    if new.userID is not null then insert into rel.usersamples(userID, sampleID) values (new.userID, new.sampleID) on conflict do nothing; end if; -- violation of "not null" is not captured by "on conflict", so test explicitly
    /* DELETE will be captured by "on delete cascade" foreign key constraint */
return null; end; $$ language plpgsql security definer;


create or replace function rel.trigger_update_userprojects_after_insert_or_update_on_projects() returns trigger as $$ begin
	/*create trigger sys_update_userprojects_after_insert_or_update_on_projects after insert or update of userID on eln.projects for each row execute function rel.trigger_update_userprojects_after_insert_or_update_on_projects();*/
	/* DELETE will be handled automatically by foreign key constraint with "on delete cascade" */
	if (tg_op = 'UPDATE') and (new.userID is distinct from old.userID) then /* will also evaluate to true for the case of new.userID is null and old.userID is null; won't harm however.*/
	   /* in the case of a simultaneously updated projectID, the FK cascade has already performed the renaming, so use new.sampleID explicitly. (confirmed by explicit test) */
        delete from rel.userprojects 
            where (rel.userprojects.userID, rel.userprojects.projectID) = (old.userID, new.projectID)  
            and not exists
               (select 1 from rel.userproposals join rel.projectproposals using (proposalID)
                 where  (rel.userproposals.userID, rel.projectproposals.projectID) = (old.userID, new.projectID));
	end if;
    if new.userID is not null then insert into rel.userprojects(userID, projectID) values (new.userID, new.projectID) on conflict do nothing; end if; -- violation of "not null" is not captured by "on conflict", so test explicitly
return null; end; $$ language plpgsql security definer;


create or replace function rel.trigger_update_usersamples_after_insert_on_userproposals() returns trigger as $$ begin
/* create trigger sys_update_usersamples_after_insert      after insert    on rel.userproposals   for each row execute function rel.trigger_update_usersamples_after_insert_on_userproposals();*/
-- execute after insert of userproposals (not necessary after updates & deletes, these will only happen in context of of FK name changes / delete, and will be handled automatically through "cascade"),
-- to that any new user-sample relations can be added, 
insert into rel.usersamples(userID, sampleID) 
    select userID, sampleID 
    from rel.userproposals join rel.proposalsamples using (proposalID)
    where proposalID = new.proposalID
    on conflict do nothing;
return null; end; $$ language plpgsql security definer;


create or replace function rel.trigger_update_usersamples_after_insert_on_proposalsamples() returns trigger as $$ begin
	/*create trigger sys_update_usersamples_after_insert      after insert    on rel.proposalsamples for each row execute function rel.trigger_update_usersamples_after_insert_on_proposalsamples();*/
-- execute after insert of proposalsamples (not necessary after updates & deletes, these will only happen in context of of FK name changes and will be handled automatically through "cascade"),
-- to that any new user-sample relations can be added, 
insert into rel.usersamples(userID, sampleID) 
    select userID, sampleID 
    from rel.userproposals join rel.proposalsamples using (proposalID)
    where sampleID = new.sampleID
    on conflict do nothing;
return null; end; $$ language plpgsql security definer;


create or replace function rel.trigger_update_userprojects_after_insert_on_userproposals() returns trigger as $$ begin
	/*create trigger sys_update_userprojects_after_insert     after insert    on rel.userproposals       for each row execute function rel.trigger_update_userprojects_after_insert_on_userproposals();*/
-- execute after insert of userprojects (not necessary after updates & deletes, these will only happen in context of of FK name changes and will be handled automatically through "cascade"),
-- to that any new user-project relations can be added, 
insert into rel.userprojects(userID, projectID) 
    select userID, projectID 
    from rel.userproposals join rel.projectproposals using (proposalID)
    where proposalID = new.proposalID
    on conflict do nothing;
return null; end; $$ language plpgsql security definer;


create or replace function rel.trigger_update_userprojects_after_insert_on_projectproposals() returns trigger as $$ begin
	/*create trigger sys_update_userprojects_after_insert     after insert    on rel.projectproposals    for each row execute function rel.trigger_update_userprojects_after_insert_on_projectproposals();*/
-- execute after insert of userprojects (not necessary after updates & deletes, these will only happen in context of of FK name changes and will be handled automatically through "cascade"),
-- to that any new user-project relations can be added, 
insert into rel.userprojects(userID, projectID) 
    select userID, projectID 
    from rel.userproposals join rel.projectproposals using (proposalID)
    where projectID = new.projectID
    on conflict do nothing;
return null; end; $$ language plpgsql security definer;


drop trigger if exists sys_update_proposalsamples_after_modification  on eln.scanlog;
drop trigger if exists sys_correct_refcount_after_FK_update_on_proposalsamples on rel.proposalsamples;
drop trigger if exists sys_update_userproposals_after_modification    on eln.proposals;
drop trigger if exists sys_update_projectproposals_after_modification on eln.proposals;
create trigger sys_update_proposalsamples_after_modification  after insert or delete or update of proposalID, sampleID  on eln.scanlog          for each row execute function rel.trigger_update_proposalsamples_after_modification_on_scanlog();
create trigger sys_correct_refcount_after_FK_update_on_proposalsamples            after update of proposalID, sampleID  on rel.proposalsamples  for each row execute function rel.trigger_correct_refcount_after_FK_update_on_proposalsamples();
create trigger sys_update_userproposals_after_modification    after insert           or update of accessUserIDs, userID on eln.proposals        for each row execute function rel.trigger_update_userproposals_after_modificaton_on_proposals();
create trigger sys_update_projectproposals_after_modification after insert           or update of projectID             on eln.proposals        for each row execute function rel.trigger_update_projectproposals_after_modification_on_proposals(); 
/* deletes, and updates of projectID or proposalID on eln.proposals / eln.projects will be captured by "on update cascade on delete cascade" FK constraint */

drop trigger if exists sys_update_usersamples_before_delete             on rel.userproposals;
drop trigger if exists sys_update_usersamples_before_delete             on rel.proposalsamples;
drop trigger if exists sys_update_usersamples_after_insert              on rel.userproposals;
drop trigger if exists sys_update_usersamples_after_insert              on rel.proposalsamples;
drop trigger if exists sys_update_usersamples_after_insert_or_update_on_samples on eln.samples;
create trigger sys_update_usersamples_before_delete    before delete    on rel.userproposals   for each row execute function rel.trigger_update_usersamples_before_delete_on_userproposals();
create trigger sys_update_usersamples_before_delete    before delete    on rel.proposalsamples for each row execute function rel.trigger_update_usersamples_before_delete_on_proposalsamples();
create trigger sys_update_usersamples_after_insert      after insert    on rel.userproposals   for each row execute function rel.trigger_update_usersamples_after_insert_on_userproposals();
create trigger sys_update_usersamples_after_insert      after insert    on rel.proposalsamples for each row execute function rel.trigger_update_usersamples_after_insert_on_proposalsamples();
-- regularly, there are only delete+inserts, but no literal (user) updates on rel.userproposals and rel.proposalsamples.proposalid, i.e., these need not to be covered. (they would only occur by manual intervetion of the db admin)
-- automatic updates due to cascades from an FK constraint are applied to all tables automatically.


create trigger sys_update_usersamples_after_insert_or_update_on_samples after insert or update of userID on eln.samples for each row execute function rel.trigger_update_usersamples_after_insert_or_update_on_samples();
-- on delete or update of sampleID   is   both captured by foreign key cascades, thus take action only for updates on userID



drop trigger if exists sys_update_userprojects_before_delete                    on rel.projectproposals;
drop trigger if exists sys_update_userprojects_after_insert                     on rel.projectproposals;
drop trigger if exists sys_update_userprojects_before_delete                    on rel.userproposals;
drop trigger if exists sys_update_userprojects_after_insert                     on rel.userproposals;
drop trigger if exists sys_update_userprojects_after_insert_or_update           on eln.projects;
create trigger sys_update_userprojects_before_delete   before delete    on rel.projectproposals    for each row execute function rel.trigger_update_userprojects_before_delete_on_projectproposals();
create trigger sys_update_userprojects_after_insert     after insert    on rel.projectproposals    for each row execute function rel.trigger_update_userprojects_after_insert_on_projectproposals();
create trigger sys_update_userprojects_before_delete   before delete    on rel.userproposals       for each row execute function rel.trigger_update_userprojects_before_delete_on_userproposals();
create trigger sys_update_userprojects_after_insert     after insert    on rel.userproposals       for each row execute function rel.trigger_update_userprojects_after_insert_on_userproposals();
create trigger sys_update_userprojects_after_insert_or_update after insert or update of userID on eln.projects for each row execute function rel.trigger_update_userprojects_after_insert_or_update_on_projects();
-- on delete or update of projectID   is   both captured by foreign key cascades, thus take action only for updates on userID



/* ############## (re-)initialize tables ###############
 * ##################################################### */

delete from rel.projectproposals;
insert into rel.projectproposals (projectID, proposalID)
    select projectID, proposalID from eln.proposals where projectID is not null on conflict do nothing;

delete from rel.proposalsamples;
insert into rel.proposalsamples(proposalID, sampleID, refcount)
    select proposalID, sampleID, count(*) from eln.scanlog where proposalID is not null and sampleID is not null group by (proposalID, sampleID)
    on conflict do nothing;
  
delete from rel.userproposals;
insert into rel.userproposals(proposalID, userID)
    select proposalID, uid from eln.proposals, lateral unnest(accessUserIDs) as uid where uid is not null
    union all
    select proposalID, userID from eln.proposals 
    on conflict do nothing;

delete from rel.usersamples;
insert into rel.usersamples (userID, sampleID)
   select userID, sampleID from rel.userproposals join rel.proposalsamples using (proposalID)
   union all
   select userID, sampleID from eln.samples
   on conflict do nothing;

delete from rel.userprojects;
insert into rel.userprojects (userID, projectID)
   select userID, projectID from rel.userproposals join rel.projectproposals using (proposalID)
   union all
   select userID, projectID from eln.projects
   on conflict do nothing;
  
