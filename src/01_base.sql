/* Catacore DB schema and constraints
 * 
 * 2023, Jonas Graetz <jonas.graetz@fau.de>
 * This work was supported by the consortium DAPHNE4NFDI in the context of the work of the NFDI e.V.
 * The consortium is funded by the DFG - project number 460248799
 * 
 * 
 * cf. relations.sql, rights.sql   for user-management       -- the db_owner MUST NOT COINCIDE with a regular user role of the database / the owner account must be a dedicated one.
 * cf. archiving.sql               for data protection
 * cf. wizards.sql                 for convenience functions
 * cf. ingestion.sql               for handling ingestor script inputs
 * cf. views.sql                   for convenient instrument-/usecase-specific views on eln.scanlog
 * 
 * 
 * eln.scanlog: list of instrument metadata (per scan(file)), user annotations, basic ELN and optional link to external ELN:
 *    - instrument: instrumentID, scanID, timestamp, instrumentMetadata (and instrumentMetadataSchemaID, currently optional)
 *    - user annotation: userID, sampleID, proposalID, datasetID, comment, metadata (arbitrary userdefined JSON structure) and metadataSchemaID
 *      + datasetID allows grouping of scans into logical datasets
 *      + proposalID allows grouping into experiment units with (see eln.proposal table) documented motivation and conclusion of an experimental session, list of participant users, and reference to the funding project
 *      + extreference to allow to link to e.g. external ELN solution
 *    - user ELN entries: regular entries with adapted scanID and no instrumentMetadata, to be used for chronological log information not directly associated with a single scan
 * eln.samples:    sample information: sampleID, userID, (PID?), JSON metadata, short-name ("title") and  plain-text logbook information ("description"),
 *                 and may link to external sample database/eln (extreference)
 * eln.proposals:  ("experiment proposal": motivation & outline, potentially also report/summary). 
 *                 The smallest proposal describes a single experimental session and can have a "parent proposal"
 *                 The largest ("long term") proposal groups a number of experiments but does not yet have its own dedicated funding (then it becomes a project)
 *                 I.e. proposals are always linked to projects ("sources of funding")
 * eln.projects:   projects are distinct from proposals by being explicitly funded. 
 *                 The largest projects are long-term funding for multiple positions and a wide topical scope. Also the base funding of an institution or chair is understood as referencable "project" in this context.
 *                 The smallest type of projects (in distinction to proposals) are short term funding for one PhD position with specific topic, contract research, or a subproject formally defined within the scope of a larger one.
 * 
 * both proposals and projects allow one level of hierarchy, i.e., the smallest scope of a proposal would be a single experiment session,
 *  and the largest scope of a proposal would be to group a number of experimental sessions without having already reached the scope of an actual funded project
 *  projects are directly related to funded research proposals, and may be subdivided into smaller projects
 *  which are still distinct from proposals in the sense that they are longer-term, less specific, and have an (official) PI
 * 
 * eln.schemata: to store JSON schemata, usable for automatic consistency checks, automatic UI generation, and automatic identification of scans/datasets adhering to a certain (user-)defined standard protocol (metadata) or distinction of e.g. datasets after instrument(&metadataschema) upgrades
 */


--https://stackoverflow.com/questions/6720050/foreign-key-constraints-when-to-use-on-update-and-on-delete
--points out [context: MySQL] that update triggers on a table will not be fired in the case of implicit cascaded updates vs. explicit UPDATE statements

/*
drop table if exists
    eln.scanlog,
    eln.samples,  
    eln.proposals, 
    eln.projects,
    eln.schemata;
*/

create schema eln;
create schema sys;

/* convenience information for the DBA: */
create view sys.connections as 
select usename as userid, count(*) as connections from pg_catalog.pg_stat_activity where datname = current_database() group by (usename, client_addr) order by userid asc;

drop view if exists eln.help;
create view eln.help as 
values 
    ('scanlog'  , 'Basic list-mode/chronological electronic labnotebook holding entries for each instrument file with instrument metadata and allowing for additional comment rows (it is to be discussed whether ordering by timestamp (subject to local machine deviations) or by sampleID (only safely assigned after an experiment, and potentially only after an explicit data-transfer to a central server). Here, user annotations on proposal, sample, dataset, description/comment aare collected along with optional JSON-structured metadata with a user-defined structure.'),  
    ('samples'  , 'holds sample information: "sampleID" - any user-chosen internal sample identifier, typically just initials with sequential number or similar. "title": brief one-line characterization of the sample as e.g. used intuitively also within internal group discussions. Potentially, anything that you intended to put into the SampleID as short descriptor rather goes here. "description": logbook information documenting information on sample creation, sample composition, treatments, characterizations such as e.g. concentration, weights, sizes.. '),
    ('proposals', 'holds information on the motivation, hypothethis, methods of experiments and brief preliminary summary of findings/observations, as well as the source of funding (projectID). Defines the involved group of users.  "proposalID": any user-chosen internal identifier, typically e.g. project initials and running number. "title": analog to a paper title. "description": analog to a paper abstract -- motivation, hypothesis and method of the present experiment. "report": brief summary of the (immediate) findings/insights after the experiments'),
    ('project',   'differs from a proposal by the fact that it is explicitly associated with funding. Provide also the required funding acknowledgement statement (for publications) here.'),
    ('metadata' , 'each table as an auxiliary JSON-valued column "metadata" which allows the user to provide any custom-structured metadata meaningful in his particular research context, assuming that a user would re-use a given structure for a larger number of experiments'),
    ('extreference', 'each table has an auxiliary column "extreference" that can hold references to external ELN solutions. Pontentially, also PID/PURL references could go here.')
    ;
 

create or replace function eln.chk_user_exists(varchar) 
   returns bool as
      'select exists (select * from pg_roles where rolname = $1 and rolcanlogin)'
   language sql stable
   returns null on null input;

create or replace function sys.db_owner()
    returns varchar as $$
        select pg_catalog.pg_get_userbyid(d.datdba) as "database_owner"
        from pg_catalog.pg_database d where d.datname = current_database()
    $$ language sql stable;

   

   
  /* convenience function used to reduce code redundancy for create commands for multiple tables or triggers */
create or replace function sys.foreach_execute(varchar[], varchar) returns void as $$
declare string varchar; begin
    foreach string in array $1 loop
       /* create policies from template expression */
       execute format($2, string);
    end loop;
end; $$ language plpgsql;


create table eln.schemata (
    "timestamp" timestamp default localtimestamp(0),          -- time of schema defintion/registration, SciCat "createdAt"
   schemaID varchar not null,     -- for schemata specific to a certain proposal, the naming schema could be ICSP-proposalID[-...][-runningNumber][-version]
                                  -- for schemata specific to a certain technique, the naming schema could be ICSP-techniqueCode[-...][-v1|2|3...]; e.g. GISAXS, GISAXS-CLINT, GISAXS-InSitu, GISAXS-InSitu-Catalysis, GISAXS-v2, etc..
                                  -- for schemata specific to a certain sample categorie, the naming schema could be ICSP-sampleCategory[-...]-runningNumber
                                  -- for schemata specific to a certain Instrument, the naming schema could be instrumentID-runningNumber
   parentSchemaID varchar,         -- for the case of schemata that are backwards-compatible, i.e., only add keys and/or do not remove required keys
   userID varchar default "current_user"(),                -- in doubt, the "corresponding author"
   title varchar generated always as ("schema" ->> 'title') stored,
   description varchar generated always as ("schema" ->> 'description') stored,
   "schema" JSON,                 -- the actual JSON schema, cf. http://json-schema.org/
   extReference varchar,          --
   primary key (schemaID),
   constraint valid_user check (eln.chk_user_exists(userID)), -- pg_roles cannot be explicitly referenced, thus use a check function. Validates only on data change, not on changes in pg_roles
   constraint disallow_self_reference check (parentSchemaID != schemaID),
   foreign key (parentSchemaID) references eln.schemata(schemaID)
   -- make a schema editable as long as it is not in use / as long as the referencing entries are still editable
   -- OR: make a certain freezing period, e.g. one month, allowing the user to invalidate his own (initial) attempts, yet avoiding a plethora of unused schemata
   -- requires a trigger 
);

create index schemata_timestamp_idx on eln.schemata("timestamp");
create index schemata_userID_idx    on eln.schemata(userID);


create table eln.projects (
    "timestamp" timestamp default localtimestamp(0), -- corresponding to "createdAt" (SciCat)
   projectID varchar not null,    -- typically, the abbreviation or "codename" of the project used in the funding proposal
   parentProjectID varchar,       -- add the possibility to define (narrow-scoped/short-term) sub-projects of a larger, wide-scoped/long-term project.
                                  -- This allows to split large funded projects into smaller logical entities; mapping e.g. to the CSMD "investigation", which allows to group multiple proposals
                                  -- use triggers to limit the allowed hierarchy depths to 1 subproject level.
   userID varchar default "current_user"(), -- "principal investigator" / "speaker" / "corresponding author"
                                  -- strictly enforce current_user to guarantee the origination/authorship of entries?
   title varchar,
   description varchar,           -- or abstract; use markdown syntax
   citation varchar,              -- how the project is to be cited/acknowledged, e.g. "DFG no. xxxxx, project-name"
   metadata JSON,                 -- custom structured JSON. Cf. SciCat
   metadataSchemaID varchar,
   extReference varchar,          -- URI, PID, filename, etc..
   primary key (projectID),
   constraint valid_userID        check      (eln.chk_user_exists(userID)), -- pg_roles cannot be explicitly referenced, thus use a check function. Validates only on data change, not on changes in pg_roles
                                   -- strictly enforce current_user to guarantee the origination/authorship of entries?
   constraint check_self_reference check     (parentProjectID != projectID),   -- disallow self references
   constraint disallow_stub_parent check     (parentProjectID not like '?:%'), -- disallow auto-generated stub entries, marked with '?:'
   foreign key (metadataSchemaID) references eln.schemata(schemaID) on update cascade,  -- limit updates to IDs that are already in use using triggers or row level policies on the referenced table
   foreign key (parentProjectID)  references eln.projects(projectID) on update cascade  -- limit updates to IDs that are already in use using triggers or row level policies on the referenced table 
);
create index projects_timestamp_idx on eln.projects("timestamp");
create index projects_userID_idx    on eln.projects(userID);


/* on changes of eln.proposal schema, consider also 04_archiving.sql, eln.chk_proposalID_is_frozen_before_insert_or_update() */
create table eln.proposals (
    "timestamp" timestamp default localtimestamp(0),        -- corresponding to "createdAt" (SciCat)
    proposalID varchar not null,  -- typically, projectID-[subprojectName-]runningNumber
    parentProposalID varchar,     -- for user convenience, to mark consecutive proposals. 
                                  -- Remove, to avoid "parallel" user-conventions on structure / to enable a unique mapping to the CSMD schema of study/project-investigation-proposal/experiment-dataset
                                  -- Note that "projectID" also allows to group related proposals
                                  -- and that projects may have one level of subprojects
    userID varchar default "current_user"(),  -- user that created the entry and wrote the description, 
                                  -- in doubt, the "corresponding author" or "principal investigator"
                                  -- strictly enforce current_user to guarantee the origination/authorship of entries?
    projectID varchar ,            -- CSMD "programme", the project that provides the funding
    title varchar,                 -- 
    description varchar,           -- or abstract; use markdown syntax
    report varchar,
    accessUserIDs varchar[],       -- group of users that can view the data; 
                           -- not validated for two reasons: unciritical if usernames don't exist.  
    metadata JSON,                 -- custom structured JSON. allow base64 encoded document? or put that in extReference?
    metadataSchemaID varchar,
    extReference varchar,          -- URI, PID, filename, base64 encoded document, etc..
    primary key (proposalID),
    constraint valid_userID        check     (eln.chk_user_exists(userID)), -- strictly enforce current_user to guarantee the origination/authorship of entries?
    constraint disallow_self_reference check    (parentProposalID != proposalID), -- disallow self references
    constraint disallow_stub_parent check (parentProposalID not like '?:%'), -- disallow auto-generated stub entries, marked with '?:'
    foreign key (metadataSchemaID) references eln.schemata(schemaID)    on update cascade, -- limit updates to IDs that are already in use using triggers or row level policies on the referenced table
    foreign key (parentProposalID) references eln.proposals(proposalID) on update cascade,
    foreign key (projectID)        references eln.projects(projectID)   on update cascade-- add a trigger to automatically insert project stubs if not existing, for user convenience
    -- eln.trigger_wiz_delete_orphane_stub_project relies on a foreign key violation on delete to implicitly test whether references still exist. Do therefore not use here "on delete set null" (there is also no good reason to do so)
);
create index proposals_timestamp_idx on eln.proposals("timestamp");
create index proposals_userID_idx    on eln.proposals(userID);

create table eln.samples (
   "timestamp" timestamp default localtimestamp(0),
   sampleID varchar,
   parentSampleID  varchar,        -- for derived samples. Allow lists here? / merge with "relatedSamplesID" field?
   -- relatedSampleIDs varchar[],     -- [leave more complex things up to the json metadata field], allow only one parent but many relations, to be coherent with IGSN
   userID varchar default "current_user"(),      -- SciCat "owner", in doubt: "corresponding user" / "local contact"; 'default current_user()' doesn't work / will yield NULL --> use quotes
                                   -- strictly enforce current_user to guarantee the origination/authorship of entries?
   --proposalID varchar,           -- relation is implicit through the proposalID in the logbook table. Put here for convenience, and use constraint?
    title varchar,                 -- short / oneliner description. Also for consistency with other tables: projects, proposals, (and schemata)
    description varchar,           -- generic description, usually brief. any more formalized structure goes into the 'metadata' field and/or the extReference.
    preview bytea,                 -- (BLOB) allows to store a (small) image/photograph
    metadata json,                 -- shall typically be written through (simple) web-UI based on a selected schema?
                                   -- any more structured metadata according to some schema, including e.g. chemical formula, substance identifiers, history entries, etc..
                                   -- cf. SciCat
    metadataSchemaID varchar, 
    extReference varchar,          -- e.g., an external labnotebook
    primary key (sampleID),
    constraint valid_userID        check     (eln.chk_user_exists(userID)), -- pg_roles cannot be explicitly referenced, thus use a check function. Validates only on data change, not on changes in pg_roles
                                   -- strictly enforce current_user to guarantee the origination/authorship of entries?
    constraint disallow_self_reference check (parentSampleID != sampleID),    -- disallow self references
    constraint disallow_stub_parent check    (parentSampleID not like '?:%'), -- disallow autogenerated stub entries marked with '?:'
    foreign key (metadataSchemaID) references eln.schemata(schemaID) on update cascade,
    foreign key (parentSampleID)   references eln.samples(sampleID) on update cascade
);
create index samples_timestamp_idx on eln.samples("timestamp");
create index samples_userID_idx    on eln.samples(userID);


/* NOTE: on schema changes of eln.scanlog, ensure to update also:
 *  view    sys.scanlog_ingestion_stage
 * function sys.trigger_scanlog_ingestion_handler() */
create table eln.scanlog (
    "timestamp" timestamp default localtimestamp(0), -- meaningful timestamp, typically the instrument-provided timestamp for the given entry. See "createdAt" and "updatedAt" columns tracking the timestamps of creation and last update of the entry
    userID varchar default "current_user"(),  -- do not use defaults here? / leave this for annotation: the correct userID will be different from the formal database user generating instrument entries here -- the instrument ingestor must explicitly proivde a userID (potentially null), as the sample/proposal entry generation wizard needs an explicit value as well
    instrumentID varchar not null, -- only instruments may add data here, and only with their ID.
                                   -- requires a custom user/role; and a practical compromise between protection of the credentials (i.e., do not store on "random" instrument-PCs?)
                                   -- essential would be that the only right of the instrument role is to add rows, and nothing else.
                                   -- if instruments are not allowed to add user/sample information, than fake data would be directly detectable at the annotation stage 
                            -- attack cases would be a denial of service by "occupying" scanIDs, worst case would be ingestion of plausible fake data; yet still the user annotation on sample data would be missing
    scanID varchar not null,       -- image-/scan-number / datafile
    sampleID varchar,              -- sampleID might alternatively be used to group files into datasets in the absence of explicit datasetID
    description varchar,           -- put descriptions here, in between other fields, as the combination "scanID, sampleID, description" is commonly the most relevant triplet for the operating user
    proposalID varchar,            -- maps to investigation, cf. workflow at HZB; in principle, one proposal could group multiple investigations (beamtimes, or experimental session, etc)
                                   -- the concept of "investigation" as direct correspondence to a "working day/period" could however help to foster documentation
    --experimentID varchar,        -- optional type of investigation (and hierarchy level below investigation)
    datasetID varchar,             -- group related files by means of a running number, dataset is smallest unit to be (later/potentially) assigned a PID
                                   -- typical usecase would be time series measurements, temperature/position scans, sets of reference images, etc
                                   -- identifier could be as simple as a running number
    preview bytea,                 -- (BLOB) allows to store a small preview thumbnail directly in the database, for user convenience
    modifiedParameters json,       -- turns out to be of high practical value, so we add it as an explicit field that can be populated by the instrument crawler, 
                            -- (as it is non-trivial/unreasonable to autogenerate this generically in SQL and from arbitrary "instrumentMetadata" JSON-objects)
    -- delayTimeMinutes float,     -- also add this field for its practical value, and require it to be provided by the instrument ingestor, in order to avoid highly instrument-specific SQL/JSON window-function trickery?
                                   -- remind though: the instrument ingestor must calculate the value "onsite", as it will not have select rights on the database for security reasons
                                   -- (the ingestor account must be considered quasi-public, given that the credentials must be hard-coded at each machine (OR at a central data storage)) 
    metadata json,                 -- custom user-metadata, based on (combination of) user-registerable usecase schemata, e.g. "GISAXS", "AFM", "soft-matter", etc..
    instrumentMetadata json,
    metadataSchemaID varchar,      -- optional user-provided metadataSchema, allows to validate "metadata" and to identify related experiments (by schema)
                                   -- use a trigger to put a default template into the meta-data field, for user convenience?
    instrumentMetadataSchemaID varchar, -- practical implication is that changes in to an instrument become easily visible, and reflected in formal schema defintions
    extReference varchar,          -- e.g., an external labnotebook
    createdAt timestamp default localtimestamp(0),
    updatedAt timestamp default localtimestamp(0),
    primary key (instrumentID, scanID),
    
    constraint  valid_userID        check     (eln.chk_user_exists(userID)), -- pg_roles cannot be explicitly referenced, thus use a check function. Validates only on data change, not on changes in pg_roles
    foreign key (metadataSchemaID)  references eln.schemata(schemaID)
                                    on update cascade,
    foreign key (instrumentMetadataSchemaID) references eln.schemata(schemaID)
                                    on update cascade,
    foreign key (sampleID)          references eln.samples(sampleID)        -- add before update/insert triggers to create missing entries ad-hoc
                                    on update cascade,                      -- do restrict the timeframe for updates; and note that update triggers might not be fired for implicit/cascaded udpates 
    foreign key (proposalID)        references eln.proposals(proposalID)    -- add before update/insert triggers to create missing entries ad-hoc
                                    on update cascade                       -- do restrict the timeframe for updates; and note that update triggers might not be fired for implicit/cascaded udpates
    -- eln.trigger_wiz_delete_orphane_stub_[proposal|sample] relies on a foreign key violation on delete to implicitly test whether references still exist. Do therefore not use here "on delete set null" (there is also no good reason to do so)
);


-- add indices for most frequent filter criteria
create index scanlog_userID_idx          on eln.scanlog(userID);
create index scanlog_proposalID_idx      on eln.scanlog(proposalID);
create index scanlog_sampleID_idx        on eln.scanlog(sampleID);
create index scanlog_instrument_time_idx on eln.scanlog(instrumentID, "timestamp");
-- index on instrumentID implicit through primary key


create or replace function sys.chk_type(varchar, varchar, varchar) returns bool as $$ declare comp bool; begin 
    /* instead of comparing explicitly with the type string, compare the outputs of pg_typeof, as types may have multiple spellings, e.g.: int, integer, int4, etc..
     * sole purpose is to raise an exception in case the cast was not successful, and to ensure that the code is not actually optimized away by the execution planner. */
	execute format($fmt$select pg_typeof('%1$s'::%2$s) = pg_typeof('%3$s'::%2$s);$fmt$, $1, $2, $3) into comp;  -- always returns true if the typecast is successful, raises an exception otherwise, indicating an invalid "value" or "default" string
	return comp;
	exception when invalid_text_representation then return false;
end; $$ language plpgsql;


drop table if exists sys.config;
create table sys.config (
    param varchar primary key,
    value varchar not null check (sys.chk_type(value, "type", "default")),
    "type" varchar not null,
    "default" varchar not null,
    description varchar
);
insert into sys.config(param, value, "type", "default", description) values 
    ('freezetime.metadata', '6 weeks', 'interval', '6 weeks', 'automatic freeze of scanlog entries and proposal description after last activity'),
    ('freezetime.report'  , '3 month', 'interval', '3 month', 'automatic freeze of proposal reports, extreference, metadata and project entries after initial creation');

create or replace function eln.sys_config(varchar) returns varchar as $$ 
    select value from sys.config where param = $1;
$$ language sql stable security definer;


--https://www.postgresql.org/docs/current/trigger-definition.html
/* could this also be implemented as check constraint on the table -- no, as constraint functions may not perfom selects on the own table
 * is now implemented as "constraint trigger", which has all properties of a regular trigger and is also listed as constraint. 
 * https://www.postgresql.org/docs/13/sql-createtrigger.html (historically: https://www.postgresql.org/docs/9.0/sql-createconstraint.html)*/
create or replace function eln.trigger_chk_limit_project_hierarchy() returns trigger as $$ begin
    /* check that:
     * - parentProject does not itself also have an parentProjectID;
     * - projectID is not already used as parentProjectID elsewhere;
     */
    if (new.parentProjectID is not null) then
        if exists (select * from eln.projects where eln.projects.projectID = new.parentProjectID and eln.projects.parentProjectID is not null)
        or exists (select * from eln.projects where eln.projects.parentProjectID = new.projectID) then
           raise exception 'Only one level of project hierachy is allowed:
The given parentProjectID refers to a project that itself already is a subproject,
or the given projectID is already referenced as parentProjectID elsewhere,
or the given parentProjectID is self-referencing the projectID.';
        end if;
    end if;
    return null; /*because this is an AFTER INSERT OR UPDATE trigger*/
end; $$ language plpgsql security definer;


create or replace function eln.trigger_chk_limit_proposal_hierarchy() returns trigger as $$ begin
    /* check that:
     * - parentProposal does not itself also have an parentProposalID;
     * - proposalID is not already used as parentProposalID elsewhere;
     */
    if (new.parentProposalID is not null) then
        if exists (select * from eln.proposals where eln.proposals.proposalID = new.parentProposalID and eln.proposals.parentProposalID is not null)
        or exists (select * from eln.proposals where eln.proposals.parentProposalID = new.proposalID) then
           raise exception 'Only one level of project hierachy is allowed:
The given parentProposalID refers to a proposal that itself already is a sub-proposal,
or the given proposalID is already referenced as parentProposalID elsewhere,
or the given parentProposalID is self-referencing the proposalID.';
        end if;
    end if;
    return null; /*because this is an AFTER INSERT OR UPDATE trigger*/
end; $$ language plpgsql security definer;


/* implement as constraint trigger so that it will be listed along with all other table constraints. Technically it is equivalent to a regular trigger (potentially deferred to the end of the transaction) */
drop trigger if exists chk_limit_project_hierarchy_after_modification       on eln.projects;
drop trigger if exists chk_limit_proposal_hierarchy_after_modification      on eln.proposals; 
create constraint trigger chk_limit_project_hierarchy_after_modification  after insert or update of parentProjectID  on eln.projects  for each row execute function eln.trigger_chk_limit_project_hierarchy();
create constraint trigger chk_limit_proposal_hierarchy_after_modification after insert or update of parentProposalID on eln.proposals for each row execute function eln.trigger_chk_limit_proposal_hierarchy();


do $do$ begin perform sys.foreach_execute('{project, proposal, sample}'::varchar[],$$

    /* trigger function template(s) */
    create or replace function eln.trigger_chk_exists_generated_stub_%1$s_entry_after_modification() returns trigger as $tf$ begin
	    if (new.%1$sID is distinct from old.%1$sID) and exists (select 1 from eln.%1$ss where %1$sID = '?:'||ltrim(new.%1$sID,'?:') and %1$sID != new.%1$sID) then
            raise exception 'a corresponding stub entry (prefixed with "?:") already exists';
        end if;
        return null; /*because this is an AFTER INSERT OR UPDATE trigger*/
    end; $tf$ language plpgsql security definer;

    /*register trigger(s) */
    drop trigger if exists chk_exists_generated_stub_%1$s_entry_after_modification on eln.%1$ss;
    create constraint trigger chk_exists_generated_stub_%1$s_entry_after_modification  after insert or update of %1$sID  on eln.%1$ss  for each row execute function eln.trigger_chk_exists_generated_stub_%1$s_entry_after_modification();

$$); end $do$;


