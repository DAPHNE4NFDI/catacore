/* (user) rights management: grants, policies and triggers.
 * 
 * 2023, Jonas Graetz <jonas.graetz@fau.de>
 * 
 * 1) instrument meta-data ingestor functional account is limited to insert on view sys.scanlog_ingestor_stage (and may currently see public tables in schema raw, used temporarily to translate (legacy) filenames to scanIDs
 * 2) users can always insert new entries in eln.samples, eln.proposals, eln.projects, eln.scanlog
 * 3) users can edit (user-)metadata on all entries they own, and on all entries that are not yet owned/claimed by anyone (through the userID field), provided the entries are not yet in "archived" or "frozen" state, see (5) 
 * 4) users can list all entries they are related to through ownership of the entry itself, through participation in a proposal related to scans and projects, or through ownership of the sample referenced in scans
 * 5) after a fixed period of inactivity, entries will be fixed/archived, and cannot be edited or deleted anymore., cf. archvining.sql
 * 
 * These rules are implemented through a combination of :
 * - grant mechanism ( (per-user, per-command, per column) access control to tables)
 * - row level policies (per-user, per-command, per row) access control to tables. 
 *     - Best suited for limiting select to user-related rows and for enforcing checks for new values on INSERT and UPDATE 
 *     - Also suited to limit updates/deletes, the user will however never get an exception/error when attempting to delete/update a row that he is able to SELECT, even if the delete/update is not in fact performed to a restricting "using_expression"
 *     - can be selectively skipped for the database / table owner
 * - triggers
 *     - allow fine-grained row & column level update/delete protections
 *     - can throw exceptions to notify the user of prohibited actions
 *     - must be explicitly disabled (for all) for the time of administrator "overrides"
 * 
 * Grating and revoking users roles and ingestor roles is managed through the tables 
 *    sys.users and
 *    sys.ingestors
 *
 * General read access to all instrument-related scanlog rows can be granted to instrument scientists noted in
 *    sys.instruments
 *
 * */

/* requires:  base.sql, relations.sql */
/* complemented by: archiving.sql */


/* useful FAQs on trigger and constraint function timing:
 * 
 * https://stackoverflow.com/questions/52565720/postgres-trigger-side-effect-is-occurring-out-of-order-with-row-level-security-s
 * https://stackoverflow.com/questions/24468064/are-constraints-executed-before-or-after-customized-trigger
 *    --> answer in https://www.postgresql.org/docs/9.1/sql-createtrigger.html#AEN68968
 */

--SELECT *
--  FROM  information_schema.role_table_grants
--  WHERE grantee ='..username..'; -- where table_name=...
  
--select * from pg_catalog.pg_policies;
--https://www.postgresql.org/docs/current/sql-grant.html
--https://stackoverflow.com/questions/17338621
--https://dba.stackexchange.com/questions/190411

/* grant or revoke for individual users */
--do $$ begin perform sys.grant_user('..username..'); end; $$;
--do $$ begin perform sys.grant_ingestor('..username..'); end; $$;

create or replace function sys.user_role_exists(varchar) 
   returns bool as
      'select exists (select * from pg_roles where rolname = $1 and rolcanlogin)'
   language sql stable
   returns null on null input;


create table sys.users(
  idmID    varchar not null,
  email    varchar not null,
  active   boolean not null default true,
  note     varchar,
  primary key (email),
  constraint valid_idmID check (sys.user_role_exists(idmID))
);

create table sys.ingestors(
  roleID       varchar not null,
  description  varchar not null,
  active       boolean not null default true,
  primary key (roleID),
  constraint valid_roleID check (sys.user_role_exists(roleID))
);

/* sys.instruments is used for the purpose to grant, for listed (instrument-responsible) users,
 *  full SELECT rights on all instrument entries in eln.scanlog */
create table sys.instruments(
  instrumentID  varchar not null,
  description   varchar,
  accessUserIDs varchar[],
  primary key (instrumentID)
);

/*realize as materiazlized view with triggered refresh as sys.instruments will be a small table with seldom modifications. */
create materialized view rel.instrumentscientists as 
    select instrumentID, unnest(accessUserIDs) as userID
    from sys.instruments;
create index instrumentusers_idx on rel.instrumentscientists(userID, instrumentID); /* the majority of users will not be part of the table, thus query against userID first */
create or replace function sys.trigger_refresh_instrumentscientists_after_modification_of_instruments() returns trigger as $$ begin
	refresh materialized view rel.instrumentscientists;
return null; end; $$ language plpgsql security definer;
drop trigger if exists refresh_instrumentscientists_after_modification on sys.instruments;
create trigger refresh_instrumentscientists_after_modification after insert or update or delete on sys.instruments for each statement execute function sys.trigger_refresh_instrumentscientists_after_modification_of_instruments();

/* grant or revoke for a list of users. Now handled through sys.users table */
/*
do $$ declare userID varchar;
userIDs varchar[] := '{role1, role2, ...}';
begin foreach userID in array userIDs loop
    --perform sys.revoke_user(userID); 
	perform sys.grant_user(userID);
end loop; end; $$; 

do $$ begin 
	--perform sys.revoke_user('mdv01');
	perform sys.grant_ingestor('mdv01'); 
end $$;
*/

/* define sys.grant_user and sys.revoke_user functions. Collect all individual grants/revokes there */
create or replace function sys.grant_user(varchar) returns void as $fn$ begin
    /* general access to the relevant database and schema: */
	execute format($f$
		grant connect                  on database %2$s to %1$s;
	    grant usage                    on schema eln, rel, usr, vws to %1$s;  -- mdv.eln raises an error.
	    --grant select on all views in schema vws to %1$s; -- "on all views" is not possible (analog to "on all tables"). will be handled by means of "grant to public" at the time of view creation. 
        /* NOTE: for postgresql < 15 (where views operate always in the owner context), this strictly requires "instead of" triggers to be in place,
         * which will be executed in the accessing users context. 
         * otherwise the view is automatically updateable in the owner/admnin context */
	    grant execute on all functions in schema eln, rel to %1$s;
	    /* will be further confined through row level policies, foreign key constraints, and triggers: */
	    grant select     on all tables in schema rel  to %1$s;
	    --revoke select on rel.proposalsamples from %1$s; grant select (proposalID, sampleID) on rel.proposalsamples to %1$s; -- hide the internal "refcount" column
	    grant select     on all tables in schema eln  to %1$s;  /* select will be further regulated by policies */
	    grant insert, update, delete  /* update and delete will be confined by triggers checking the userID, and delete will be constrained by foreign key relations if the respective entry has already been referenced elsewhere */
	    	on table eln.projects, eln.proposals, eln.samples, eln.schemata to %1$s; -- fine grained control is added through row policies and foreign key constraints
	    --revoke insert on eln.projects from %1$s;  -- only grant this explicity to PI's. Allow for updates however in case a PI transfers ownership
	    grant update (instrumentID, scanID, userID, sampleID, description, proposalID, datasetID, metadata, metadataSchemaID, extreference)
	    	on table eln.scanlog                  to %1$s;  -- update on instrumentID required to allow user-corrections on manually inserted rows (annotations) before a scan was finished & inserted by the instrument / limit by checking whether instrumentmetadata has already been provided (indicating instrument interaction)
	    grant insert (timestamp, instrumentID, scanID, userID, sampleID, description, proposalID, datasetID, metadata, metadataSchemaID, extreference)
	        on table eln.scanlog                  to %1$s;
	    grant delete on table eln.scanlog         to %1$s; -- limit with row level policy to entries where instrumentmetadata is null, i.e., user-created entries
	    $f$, $1, current_database());
end; $fn$ language plpgsql;
--https://www.postgresql.org/docs/current/role-removal.html
create or replace function sys.revoke_user(varchar) returns void as $fn$ begin
    /* general access to the relevant database and schema: */
	execute format($$
	    --drop owned by %1$s; -- only with superuser rights
        revoke all privileges on all tables in schema eln, rel, usr, sys, vws, public from %1$s; -- prepending database, e.g. "mdv.eln", raises an error.
        revoke usage                        on schema eln, rel, usr, sys, vws, public from %1$s;
        revoke execute     on all functions in schema eln, rel, usr, sys, vws, public from %1$s;
        revoke connect                    on database %2$s from %1$s;
	    $$, $1, current_database());
end; $fn$ language plpgsql;


create or replace function sys.grant_ingestor(varchar) returns void as $fn$ begin
    /* general access to the relevant database and schema: */
	execute format($$
	    grant                  connect on database %2$s to %1$s;
        revoke all on all tables in schema eln, rel, sys from %1$s;

       grant usage on schema sys to %1$s;
       grant select on sys.scanlog_ingestion_stage to %1$s;
       grant insert on sys.scanlog_ingestion_stage to %1$s;
	    $$, $1, current_database());
end; $fn$ language plpgsql; 


alter table rel.userproposals   enable row level security;     /* "no force row level security" implied, i.e., does not apply to DB owner. This is essential for the bookkeeping work */
alter table rel.proposalsamples enable row level security;
alter table rel.projectproposals enable row level security;
alter table rel.userprojects    enable row level security;
alter table rel.usersamples     enable row level security;
alter table eln.proposals       enable row level security;
alter table eln.scanlog         enable row level security;
alter table eln.projects        enable row level security;
alter table eln.samples         enable row level security;
/* DO NOT force row level security for the db_owner, as this breaks triggers testing for the existence of entries :
alter table eln.proposals       force  row level security;
alter table eln.scanlog         force  row level security;
alter table eln.projects        force  row level security;
alter table eln.samples         force  row level security;
*/


create or replace function sys.trigger_grant_revoke_user_after_modification_on_users() returns trigger as $$ begin
	if (tg_op in ('UPDATE', 'DELETE') and not exists (select 1 from sys.users where sys.users.idmID = old.idmID and sys.users.active = true and sys.users.email is distinct from new.email)) then 
        perform sys.revoke_user(old.idmID);
       
        /* TODO: temporary solution in order to allow regular users to be also listed among ingestor accounts / to be able to run ingestor scripts. 
         *       typically, ingestor should only be one or few functional accounts used by machine ingestor scripts
         * see also INSERT/UPDATE block further down */
        if exists (select 1 from sys.ingestors where sys.ingestors.roleid = old.idmID and sys.ingestors.active = true) then
            /*re-enable ingestor rights if they had been granted*/
            perform sys.grant_ingestor(old.idmID);
        end if;
    end if;
   /* TODO: for not yet understood reasons, UPDATE from old.active = false to new.active = true does apparently not call "sys.grant_user", ie, seems not to reach this branch:*/
	if (tg_op in ('INSERT', 'UPDATE') and new.active = true) then 
        perform sys.grant_user(new.idmID);
       
        /* TODO: temporary solution in order to allow regular users to be also listed among ingestor accounts / to be able to run ingestor scripts. 
         *       typically, ingestor should only be one or few functional accounts used by machine ingestor scripts 
         * see also UPDATE/DELETE block further up */
        if exists (select 1 from sys.ingestors where sys.ingestors.roleid = new.idmID and sys.ingestors.active = true) then
            /*re-enable ingestor rights (on 'UPDATE')dbea if they had been granted*/
            perform sys.grant_ingestor(new.idmID);
        end if;
    end if;
return null; end; $$ language plpgsql; -- explicitly leave 'security definer', as this trigger shall only be executed by the db owner

create or replace function sys.trigger_grant_revoke_ingestor_after_modification_on_ingestors() returns trigger as $$ begin
	if (tg_op in ('UPDATE', 'DELETE') and (new.roleID is distinct from old.roleID or new.active = false)) then 
        perform sys.revoke_user(old.roleID); /*revoke all rights*/
        
        /* TODO: temporary solution in order to allow regular users to be also listed among ingestor accounts / to be able to run ingestor scripts. 
         *       typically, ingestor should only be one or few functional accounts used by machine ingestor scripts
         * see also INSERT/UPDATE block further down */
        if exists (select 1 from sys.users where sys.users.idmID = old.roleID and sys.users.active = true) then 
            /*re-enable user rights if they had been granted*/
            perform sys.grant_user(old.roleID); 
        end if;
    end if;
	if (tg_op in ('INSERT', 'UPDATE') and new.active = true) then 
        perform sys.grant_ingestor(new.roleID);
       
        /* TODO: temporary solution in order to allow regular users to be also listed among ingestor accounts / to be able to run ingestor scripts. 
         *       typically, ingestor should only be one or few functional accounts used by machine ingestor scripts 
         * see also UPDATE/DELETE block further up */
        if exists (select 1 from sys.users where sys.users.idmID = new.roleID and sys.users.active = true) then 
            /*re-enable user rights (on 'UPDATE') if they had been granted*/
            perform sys.grant_user(new.roleID); 
        end if;
    end if;
return null; end; $$ language plpgsql; -- explicitly leave 'security definer', as this trigger shall only be executed by the db owner


create trigger trigger_grant_revoke_user_after_modification after insert or delete or update of idmID, active on sys.users for each row execute function sys.trigger_grant_revoke_user_after_modification_on_users();
create trigger trigger_grant_revoke_user_after_modification after insert or delete or update of roleID, active on sys.ingestors for each row execute function sys.trigger_grant_revoke_ingestor_after_modification_on_ingestors();



/* --replaced with row level policy; note however that the "using" restriction of a row level policy will not raise exceptions, i.e., the user won't notice that the update wasn't effective.
create or replace function eln.trigger_check_update_permission() returns trigger as $$ begin
    if (new.userID is distinct from old.userID) and (old.userID is not null) and (old.userID is distinct from current_user) then
        raise exception 'Only the owner of this entry may submit changes';
    end if;
    return new;
end; $$ language plpgsql;
*/

/* --direct ownership transferral currently prohibited by row level security (users can only insert their own userID);
 * -- users can however reset the userID to NULL to make the dataset "claimable" by other users again.
create or replace function eln.trigger_confirm_ownership_transferral() returns trigger as $tf$ begin
    if new.userID is not null then 
        if new.userID not like '!%' and new.userID != current_user and not (old.instrumentmetadata is null and new.instrumentmetadata is not null) then
            raise exception 'In order to transfer ownership of a dataset, please explicitly prepend an exclamation mark (!).
Be aware that you loose rights to edit the datasets and potentially to view the dataset.';
        else
            new.userID := ltrim(new.userID, '!');
        end if;
    end if;
return new; end; $tf$ language plpgsql;
*/

do $do$ begin perform sys.foreach_execute('{projects, proposals, samples, schemata, scanlog}'::varchar[], $$
   drop trigger if exists check_update_permission on eln.%1$s;
    --create trigger check_update_permission  before update on eln.%1$s for each row execute function sys.trigger_check_update_permission();
$$); end; $do$;
do $do$ begin perform sys.foreach_execute('{projects, proposals, samples, schemata, scanlog}'::varchar[], $$
  	drop trigger if exists confirm_ownership_transferral on eln.%1$s;
	--create trigger confirm_ownership_transferral before insert or update of userID on eln.%1$s for each row execute function eln.trigger_confirm_ownership_transferral();
$$); end; $do$;



--https://www.postgresql.org/docs/current/sql-createpolicy.html
-- view active policies: select * from pg_catalog.pg_policies; 
-- and                   select * from pg_catalog.pg_policy;


/* add permissive policies 
 * to tables eln.projects, eln.proposals, eln.samples, eln.schemata
 * for
 * - inserts by public
 * - updates by owner, or by public if owner is not yet specified
*/

do $do$ begin perform
sys.foreach_execute('{eln.projects, eln.proposals, eln.samples, eln.schemata}'::varchar[], $$
      drop policy if exists auth_insert_all on %1$s;
      drop policy if exists auth_update_owner on %1$s;
      drop policy if exists auth_delete_owner on %1$s;
      create policy auth_insert_all on %1$s for insert with check (userID is null or userID in (current_user, 'public'));    --
      create policy auth_update_owner on %1$s for update using (userID in (current_user) or userID is null) with check (userID is null or userID in (current_user, 'public'));   -- TODO: limit updates of userID to current_user and null, i.e., no one-sided ownership transferral? could however break the ingestor user & script
      create policy auth_delete_owner on %1$s for delete using (userID = current_user); -- delete will still be prohibited by foreign key constraint if the respective entry is already referenced elsewhere
$$::varchar); end $do$;

drop policy if exists auth_select_all on eln.schemata;
create policy auth_select_all on eln.schemata for select;

drop policy if exists auth_owner          on eln.samples;
drop policy if exists auth_proposal_group on eln.samples;
create policy auth_owner           on eln.samples for select using (userID in (current_user, 'public') or userID is null);
create policy auth_proposal_group  on eln.samples for select using (exists (select 1 from rel.usersamples where (rel.usersamples.userID, rel.usersamples.sampleID) = (current_user, eln.samples.sampleID)));

drop policy if exists auth_owner          on eln.projects; /* will also be included in proposal_group, test explicitly however as backup in case of corrupted relational tables */
drop policy if exists auth_proposal_group on eln.projects;
create policy auth_owner          on eln.projects for select using (userID in (current_user, 'public') or userID is null);
create policy auth_proposal_group on eln.projects for select using (exists (select 1 from rel.userprojects where (rel.userprojects.userID, rel.userprojects.projectID) = (current_user, eln.projects.projectID)));

drop policy if exists auth_owner          on eln.proposals; /* quickest test, and required if neither sampleID/proposalID is given, or in the case of corrupted relational tables*/
drop policy if exists auth_proposal_group on eln.proposals;
create policy auth_owner          on eln.proposals for select using (userID in (current_user, 'public') or userID is null);
create policy auth_proposal_group on eln.proposals for select using (exists (select 1 from rel.userproposals where (rel.userproposals.proposalID, rel.userproposals.userID) = (eln.proposals.proposalID, current_user)));

drop policy if exists auth_owner          on eln.scanlog; /* quickest test, and required if neither sampleID/proposalID is given, or in the case of corrupted relational tables*/
drop policy if exists auth_instrument_scientist on eln.scanlog; 
drop policy if exists auth_sample_group   on eln.scanlog; /* sample group is equivalent to proposalgroup + sample owner */
drop policy if exists auth_proposal_group on eln.scanlog; /* proposal group will only be tested if sampleID is null */
create policy auth_owner     on eln.scanlog for select using (userID in (current_user, 'public') or userID is null);
create policy auth_instrument_scientist on eln.scanlog for select using (exists (select 1 from rel.instrumentscientists where (rel.instrumentscientists.userID, rel.instrumentscientists.instrumentID) = (current_user, eln.scanlog.instrumentID)));
/*only check for proposal_group on eln.scanlog if no sample is given. Otherwise, the proposal group (+ the sample owner) will be captured through the auth_sample_group policy, and the (time for the) lookup to rel.userproposals can be skipped*/
create policy auth_proposal_group on eln.scanlog   for select using (eln.scanlog.sampleID is null and exists (select 1 from rel.userproposals where (rel.userproposals.proposalID, rel.userproposals.userID) = (eln.scanlog.proposalID, current_user)));
/* sample_group is equivalent to proposal_group + sample_owner; whether eln.scanlog should be controlled over proposal_group or sample_group also depends on the question which value might be left NULL */
create policy auth_sample_group on eln.scanlog for select using (exists (select 1 from rel.usersamples where (rel.usersamples.userID, rel.usersamples.sampleID) = (current_user, eln.scanlog.sampleID)));

drop policy if exists auth_delete_owner on eln.scanlog;
create policy auth_delete_owner on eln.scanlog using ((userID = current_user) and (instrumentmetadata is null)); -- corresponds to a manually inserted entry
/* instrument columns are blocked through grant / column level rules; TODO: currently grant allows also instrument columns, to allow for custom / virtual instruments */
drop policy if exists auth_insert_all on eln.scanlog; 
drop policy if exists auth_update_owner on eln.scanlog;
create policy auth_insert_all on eln.scanlog as permissive for insert to public with check (true); /* grant provides enough limitations */
create policy auth_update_owner on eln.scanlog as permissive for update to public using (userID = current_user or userID is null) with check (userID = current_user or userID is null); 


/* use trigger to selectively allow UPDATE on InstrumentID, ScanID for user-generated entries without instrument information. DELETE (when instrumendMetatata is not null) is already limited by row level policy  */
create or replace function eln.trigger_chk_protect_instrument_entries_on_update_on_scanlog() returns trigger as $$ begin
	    /* fine grained row policy to allow for user-updates on instrument-ID and scan-ID for manually inserted rows (prior to a scan, e.g.) if no instrument metadata has been added yet, 
	     * i.e., as long as the (instrumentID, scanID) has not yet been used by an instrument (ingestor) / associated with actual instrumentmetadata */
	    /* "instrumentID, scanID are guaranteed to be not null, as it is part of the primary key */
    	if (old.instrumentmetadata is not null) and (new.scanID, new.instrumentID) is distinct from (old.scanID, old.instrumentID) then 
        	raise exception 'instrument provided (instrumentID, scanID) cannot be re-assigned';
        end if;
return new; end;
$$ language plpgsql stable;

drop trigger if exists chk_protect_instrument_entries_before_update             on eln.scanlog;
create trigger chk_protect_instrument_entries_before_update             before update of instrumentID, scanID on eln.scanlog for each row execute function eln.trigger_chk_protect_instrument_entries_on_update_on_scanlog();


do $do$ begin perform sys.foreach_execute('{rel.userproposals, rel.userprojects, rel.usersamples}'::varchar[],$$
    -- implies: "as permissive [for select] to public"
   drop policy if exists auth_user on %1$s;
   create policy auth_user on %1$s for select using (userID in (current_user, 'public')); /* the user should also always be captured by the relations in schema rel, ensure however explicitly to avoid perceived data loss in case of inconsistencies in schema rel.*/
$$); end $do$;

drop policy if exists auth_sampleusers    on rel.proposalsamples ;
drop policy if exists auth_proposal_group on rel.projectproposals;

/* allowing sampleusers as opposed to proposalusers includes the corner case of a sample owned by a person, measured by somebody else in context of a proposal the user is not included in.*/
create policy auth_sampleusers    on rel.proposalsamples  for select using (exists (select 1 from rel.usersamples   where (rel.usersamples.userID ,  rel.usersamples.sampleID)     = (current_user, rel.proposalsamples.sampleID   ))); /* this by design involves also all proposalusers*/
create policy auth_proposal_group on rel.projectproposals for select using (exists (select 1 from rel.userproposals where (rel.userproposals.userID, rel.userproposals.proposalID) = (current_user, rel.projectproposals.proposalID)));

--create policy auth_user_group     on rel.projectproposals for select using (exists (select 1 from rel.userprojects where (rel.userprojects.userID, rel.userprojects.projectID) = (current_user, rel.projectproposals.projectID)));
drop policy if exists   auth_user_group     on rel.projectproposals; -- shows all proposals (even those a user is not part of), if the user has become part of the project

